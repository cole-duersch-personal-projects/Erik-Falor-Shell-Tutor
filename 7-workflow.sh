#!/bin/sh


_HELP="Lesson #7 Topics
================
* Review of skills learned in other lessons

Commands used in this lesson
============================
* git
* cd
* ls
* less
* nano
* cp
"

# Put tutorial library files into $PATH
[[ -z $_TUTR ]] && PATH=$PWD/.lib:$PATH

. shell-compat-test.sh

source ansi-terminal-ctl.sh
# This function is named `_Git` to avoid clashing with Zsh's `_git`
_Git() { (( $# == 0 )) && echo $(blu Git) || echo $(blu $*); }
_local() { (( $# == 0 )) && echo $(ylw local) || echo $(ylw $*); }
_remote() { (( $# == 0 )) && echo $(mgn remote) || echo $(mgn $*); }
_origin() { (( $# == 0 )) && echo $(red origin) || echo $(red $*); }
_md() { (( $# == 0 )) && echo $(blu Markdown) || echo $(blu $*) ; }
_code() { (( $# == 0 )) && echo $(cyn code) || echo $(cyn "$*"); }
_py() { (( $# == 0 )) && echo $(grn Python) || echo $(grn $*) ; }

source record.sh
if [[ -n $_TUTR ]]; then
	source generic-error.sh
	source git.sh
	source nonce.sh
	source open.sh

	# Open the current Git repo's origin web page
	browse_repo() {
		_tutr_git_repo_https_url
		if [[ -n $REPLY ]]; then
			_tutr_open $REPLY
			_tutr_warn echo "Opening $REPLY in your web browser..."
		else
			_tutr_warn echo "Failed to find this repo's origin URL!"
		fi
	}
fi


_finish6() {
	cat <<-:
	It looks like you haven't finished lesson $(bld 6-git.sh).  You must complete
	that lesson before you can start this one.

	If you have gotten this message in error, please contact
	$(cyn erik.falor@usu.edu) for help.
	:
}

_repo_missing() {
	cat <<-:
	I could not find the starter code repository you cloned in the previous
	lesson $(bld 6-git.sh).  This lesson cannot continue without it.

	It should be in the parent directory and be named either
	$(path cs1440-falor-erik-assn0) or $(path cs1440-assn).

	If that repository exists but is under a different name, rename it back
	to $(path cs1440-falor-erik-assn0) or $(path cs1440-assn) with $(cmd "mv [OLD] [NEW]").
	Then restart this lesson.

	If you have gotten this message in error, please contact
	$(cyn erik.falor@usu.edu) for help.
	:
}

_repo_bad_origin() {
	(( $# != 1 )) && _tutr_die echo Usage: _repo_missing DIRECTORY
	cat <<-:
	I found a $(_Git) repository at the path
	  $(path $1),
	but its $(_origin) remote is not set up as it should have been at by end of
	the previous lesson $(bld 6-git.sh).

	To fix it, you can delete and re-clone that repository from your account
	on GitLab.

	If you need help, please contact $(cyn erik.falor@usu.edu).
	:

}

_origin_not_eriks() {
	(( $# != 1 )) && _tutr_die echo Usage: _origin_not_eriks DIRECTORY
	( # create a sub shell to not leave the tutorial in the wrong PWD
		cd "$1"
		[[ $(git remote get-url origin) != *erik.falor* ]]
	)
}

_python3_not_found() {
	cat <<-PNF
	I could not find a working Python 3 interpreter on your computer.
	It is required for this lesson.

	Contact erik.falor@usu.edu for help
	PNF
}

setup() {
	if _tutr_record_exists ${_TUTR#./}; then
		_tutr_warn printf "'You have already completed this lesson'"
		if ! _tutr_yesno "Would you like to do it again?"; then
			_tutr_info printf "'SEE YOU SPACE COWBOY...'"
			exit 1
		fi
	fi
	source screen-size.sh 80 30

	export _BASE=$PWD
	# Do this the hard way because I can't count on GNU Coreutils
	# realpath(1) or readlink(1) on all systems
	export _PARENT="$(cd .. && pwd)"
	local _LONG="$_PARENT/cs1440-falor-erik-assn0"
	local _SHORT="$_PARENT/cs1440-assn0"
	export _REPO="$_SHORT"

	# Bail out unless the last lesson was completed AND the starter code
	# repo exists AND has an 'origin' that DOES NOT point back to my account
	# Otherwise, note where that repo is located
	if [[ ! -s .6-git.sh ]]; then
		_tutr_die _finish6
	elif [[ ! -d "$_LONG/.git" && ! -d "$_SHORT/.git" ]]; then
		_tutr_die _repo_missing
	elif [[ -d "$_SHORT/.git" ]]; then
		if ! _origin_not_eriks "$_SHORT"; then
			_tutr_die _repo_bad_origin "$_SHORT"
		fi
	elif [[ -d "$_LONG/.git" ]]; then
		if ! _origin_not_eriks "$_LONG"; then
			_tutr_die _repo_bad_origin "$_LONG"
		fi
	fi

	source assert-program-exists.sh
	_assert_program_exists nano
	_assert_program_exists less

	if   which python &>/dev/null && [[ $(python -V 2>&1) = "Python 3"* ]]; then
		export _PY=python
	elif which python3 &>/dev/null && [[ $(python3 -V 2>&1) = "Python 3"* ]]; then
		export _PY=python3
	else
		_tutr_die _python3_not_found
    fi
}


cleanup() {
	# Remember that this lesson has been completed
	echo "You worked on Lesson #7 for $(_tutr_pretty_time)"
}


prologue() {
	[[ -z $DEBUG ]] && clear
	echo
	cat <<-PROLOGUE
	Shell Lesson #7: The lesson that really tied the room together

	This lesson reviews all of the skills learned in the other lessons.

	Let's get started!

	PROLOGUE

	_tutr_pressanykey
}


# Rename the starter code repo from cs1440-falor-erik-assn0 to cs1440-assn0
rename_repo_rw() {
	command mv cs1440-assn0 cs1440-falor-erik-assn0
	cd "$_BASE"
}

rename_repo_ff() {
	cd "$_PARENT"
	command mv cs1440-falor-erik-assn0 cs1440-assn0
}

rename_repo_pre() {
	export _SRC=cs1440-falor-erik-assn0
}

rename_repo_prologue() {
	cat <<-:
	Go up and out of this directory and rename your starter code repo
	  $(path cs1440-falor-erik-assn0)
	to
	  $(path cs1440-assn0)
	:
}

# If they name the repo the wrong thing, try to guide them back
rename_repo_test() {
	_TARGET_IS_NOT_GIT_REPO=99
	_INCORRECT_DEST=98
	if   [[ -d "$_REPO/.git" ]]; then return 0
	elif [[ -d "$_REPO" ]]; then return $_TARGET_IS_NOT_GIT_REPO
	elif [[ ${_CMD[0]} == mv && ${_CMD[1]} == *$_SRC && $_RES == 0 ]]; then
		_SRC=${_CMD[2]}
		return $_INCORRECT_DEST
	elif [[ "$PWD" != "$_PARENT" ]]; then return $WRONG_PWD
	elif _tutr_nonce cd; then return $PASS
	else _tutr_generic_test -c mv -a "$_SRC|$_SRC/" -a "cs1440-assn0|cs1440-assn0/"
	fi
}

rename_repo_hint() {
	case $1 in
		$PASS) ;;
		$WRONG_PWD) _tutr_minimal_chdir_hint "$_PARENT" ;;
		$_TARGET_IS_NOT_GIT_REPO)
			cat <<-:
			Something has gone wrong and the directory $(path cs1440-assn0) is not a
			$(_Git) repository.

			The lesson cannot proceed until this is fixed.  If you need help
			with this, please reach out to $(cyn erik.falor@usu.edu).
			:
			;;

		$_INCORRECT_DEST)
			cat <<-:
			Oops!  It looks like you renamed $(path ${_CMD[1]})
			to
			  $(path ${_CMD[2]})
			instead of
			  $(path cs1440-assn0).

			That's okay, you can always try again.  This time, try:
			  $(cmd mv $_SRC cs1440-assn0)
			:
			;;

		*)
			_tutr_generic_hint $1 mv "$_PARENT"
			cat <<-:

			Rename the starter code repo $(path $_SRC)
			to
			  $(path cs1440-assn0)
			:
			;;
	esac
}



# cd into the repo
cd_repo_rw() {
	cd "$_REPO"
}

cd_repo_ff() {
	cd "$_PARENT"
}

cd_repo_prologue() {
	if [[ $PWD == $_BASE ]]; then
		cat <<-:
		Change to the starter code repository at $(path ../$(basename $_REPO))
		:
	else
		cat <<-:
		Now change to directory you just renamed.
		:
	fi
}

cd_repo_test() {
	[[ "$PWD" == "$_REPO" ]] && return 0
	return $WRONG_PWD
}

cd_repo_hint() {
	_tutr_generic_hint $1 cd "$_REPO"
}



ls0_pre() {
	_tutr_git_repo_https_url
	export _REPO_HTTPS=$REPLY
	browse_repo
}

ls0_prologue() {
	cat <<-:
	I'm opening your repository on GitLab in a browser tab (if it doesn't
	come up, browse directly to $(path $_REPO_HTTPS)).

	Here in the terminal I want you to run $(cmd ls) and have a look at the
	files and folders you can see here.  Compare that with what you see in
	your browser.
	:
}

ls0_test() {
	_tutr_generic_test -c ls -x -d "$_REPO"
}

ls0_hint() {
	_tutr_generic_hint $1 ls "$_REPO"
}

ls0_epilogue() {
	_tutr_pressanykey

	cat <<-:

	You will notice that the same files and directories that $(cmd ls) shows here
	in the terminal also appear on GitLab (with the small exception that
	$(path .gitignore) is not hidden on the website).  This is because at this
	moment in time both the $(_local) and $(_remote) repositories are identical
	to each other.  As you work in $(_local this) repository it will begin to
	differ from the $(_remote).

	In this lesson you will make several commits in $(_local this repository) and push
	them up to the $(_remote).  I want you to get into the habit of checking
	with GitLab to make sure that all the files and changes that you intend
	to push arrive on GitLab just as you expect.  Because we can only grade
	what you push to GitLab, it is very important that you are $(bld always) aware
	of what state your $(_remote remote repository) is in.

	:

	_tutr_pressanykey
}



# look at instructions/README.md both in browser and terminal
view_instructions_rw() {
	cd "$_REPO"
}

view_instructions_ff() {
	cd "$_REPO/instructions"
}

view_instructions_prologue() {
	cat <<-:
	Take a closer look at the contents of the $(path instructions) directory.
	Specifically, I want you to look at $(_md instructions/README.md)
	both in the terminal with $(cmd less) and in the browser.
	:
}

view_instructions_test() {
	if [[ "$PWD" == "$_REPO" ]]; then
		_tutr_generic_test -c less -a instructions/README.md -d "$_REPO"
	else
		_tutr_generic_test -c less -a README.md -d "$_REPO/instructions"
	fi
}

view_instructions_hint() {
	_tutr_generic_hint $1 less "$_REPO/README.md"

	cat <<-:

	Look at the file $(_md README.md) in the $(path instructions) directory.
	:
}

view_instructions_epilogue() {
	cat <<-:
	Don't you think that looks pretty nice on the website?

	Because that file is written in the $(_md) format, GitLab is able to
	automatically reformat it for the web.  For this reason you will write
	all of your project documentation in $(_md) in CS 1440.  $(_md) is
	designed to be easy to write and read in its source form.  This gives
	$(_md) a big advantage over other document languages like HTML.

	:
	_tutr_pressanykey
}



# look at instructions/Markdown.md both in browser and terminal
view_markdown_md_rw() {
	cd "$_REPO/instructions"
}

view_markdown_md_ff() {
	cd "$_REPO/instructions"
}

view_markdown_md_prologue() {
	cat <<-:
	$(_md) is very easy to learn, and I think you will enjoy writing
	documents this way.  You only need to learn a handful of patterns to use
	it.  I've prepared a helpful document in this directory to help you get
	started.

	Look for a file here named $(_md Markdown.md) and open it in $(cmd less) and, at the
	same time, your browser.  This will let you see how each part of the
	source document is converted by GitLab into a pretty webpage.

	You'll notice that this document repeats itself.  This is intentional.

	The $(bld first passage) is converted by GitLab into pretty HTML, and the
	$(bld second) shows what the corresponding source looks like.  This is to help
	readers of the website easily see what the $(_md) syntax looks like.
	:
}

view_markdown_md_test() {
	if [[ "$PWD" == "$_REPO" ]]; then
		_tutr_generic_test -c less -a instructions/Markdown.md -d "$_REPO"
	else
		_tutr_generic_test -c less -a Markdown.md -d "$_REPO/instructions"
	fi
}

view_markdown_md_hint() {
	_tutr_generic_hint $1 less "$_REPO/instructions"

	cat <<-:

	Look at the file $(_md Markdown.md) in the $(path instructions) directory.
	:
}



# navigate to ../doc; view Plan.md
# view Plan.md in both browser and terminal
# Notice how Markdown features are used in DuckieCorp documentation

view_plan_rw() {
	cd "$_REPO/instructions"
}

view_plan_ff() {
	cd "$_REPO/doc"
}

view_plan_prologue() {
	cat <<-:
	Now that you've seen how a few document features are achieved in
	Markdown, go up and over into $(path doc) and look closely at $(_md Plan.md).
	Pay attention to the places where I use:

	  * Headings
	  * Bold text
	  * Italicized text
	  * Bulleted list
	:
}

view_plan_test() {
	if [[ "$PWD" == "$_REPO" ]]; then
		_tutr_generic_test -c less -a doc/Plan.md -d "$_REPO"
	else
		_tutr_generic_test -c less -a Plan.md -d "$_REPO/doc"
	fi
}

view_plan_hint() {
	_tutr_generic_hint $1 less "$_REPO/doc"

	cat <<-:

	Look at the file $(_md Plan.md) in the $(path doc) directory.
	:
}

view_plan_epilogue() {
	cat <<-:
	Does that make sense?

	Now it's your turn to write some $(_md)!

	:
	_tutr_pressanykey
}




edit_readme_rw() {
	git restore "$_REPO/README.md"
	cd "$_REPO/doc"
}

edit_readme_ff() {
	cd "$_REPO"
	cat <<-':' > "$_REPO/README.md"
	# A first level-heading

	## A second level heading

	This paragraph contains **bold** text for your enjoyment.

	*   This is a bulleted list
	*   The Software Development Plan (`Plan.md`) describes the process you followed
	*   The Spring Signature (`Sprint.md`) records how you spent your time
	*   Other documentation you are asked to create for the project is placed in this directory

	```
	Pre-formatted code blocks
	look like this
	```
	:
}

edit_readme_prologue() {

	cat <<-:
	Navigate back to the top directory of the repository (i.e. where the
	directories $(path doc), $(path instructions) and $(path src) are located) and open the file
	$(_md README.md) in $(cyn Nano).

	Add following $(_md) features to this file:

	  * Headings (both $(bld level 1) and $(bld level 2))
	  * $(bld Bold) text
	  * $(bld Bulleted list)
	  * $(bld Inline code) (not a code block)
	  * $(bld Pre-formatted) block (a.k.a. a code block)
	:
}

edit_readme_test() {
	_H1=99
	_H2=98
	_BOLD=97
	_BLIST=96
	_INLINE=95
	_BLOCK=94
	_CHANGE=93
	_README_MISSING=92

	[[ "$PWD" != "$_REPO" ]] && return $WRONG_PWD
	[[ ! -f "$_REPO/README.md" ]] && _tutr_file_clean README.md && return $_README_MISSING
	_tutr_file_clean README.md && return $_CHANGE

	# what about tabs?
	command grep -qE '^#[ 	]..*$' README.md
	local _NEED_H1=$?
	command grep -qE '^##[ 	]..*$' README.md
	local _NEED_H2=$?
	command grep -qE '\*\*.*\*\*' README.md
	local _NEED_BOLD=$?
	command grep -qE '^\*[  ]..*$|^\+[  ]..*$|^-[    ]..*$' README.md
	local _NEED_BLIST=$?
	command grep -qE '`..*`' README.md
	local _NEED_INLINE=$?
	local fences=$(grep -cE '^```|^~~~' README.md)
	(( fences >= 2 && fences % 2 == 0 ))
	local _NEED_BLOCK=$?

	if   (( _NEED_H1 )); then return $_H1
	elif (( _NEED_H2 )); then return $_H2
	elif (( _NEED_BOLD )); then return $_BOLD
	elif (( _NEED_BLIST )); then return $_BLIST
	elif (( _NEED_INLINE )); then return $_INLINE
	elif (( _NEED_BLOCK )); then return $_BLOCK
	elif (( ( _NEED_H1 & _NEED_H2 & _NEED_BOLD & _NEED_BLIST & _NEED_INLINE & _NEED_BLOCK ) == 0)); then return 0
	elif _tutr_nonce; then return $PASS
	else _tutr_generic_test -c nano -a README.md -d "$_REPO"
	fi
}

edit_readme_hint() {
	case $1 in
		$_H1)
			cat <<-:
			Add a level 1 heading to $(_md README.md), and save the file.

			This is done with a line that begins with a single $(_code "#").
			Make sure there is a space between the $(_code "#") and the text that follows.
			:
			;;

		$_H2)
			cat <<-:
			Add a level 2 heading to $(_md README.md), and save the file.

			This is done with a line that begins with two $(_code "##")s.
			Make sure there is a space between the $(_code "##")s and the text that follows.
			:
			;;

		$_BOLD)
			cat <<-:
			Add some text to $(_md README.md) that will come out in $(bld bold).

			Surround one or more words with $(bld two asterisks) $(_code "**").

			Make sure that there is no blank space between the $(_code "**")s, and that the $(_code "**")s
			touch the words they are making bold.

			Not this:

			  $(_code "** this is wrong **")
			  $(_code "**this is wrong **")
			  $(_code "** this is wrong**")
			  $(_code "* *this is wrong* *")

			But like this:
			  $(_code "**this is right**")

			:
			;;

		$_BLIST)
			cat <<-:
			Add a $(bld bulleted list) to $(_md README.md).

			Begin a line with an asterisk $(_code "*"), followed by a $(bld space) and $(bld some text).
			The first bullet should be on the left margin (i.e., not indented).
			:
			;;

		$_INLINE)
			cat <<-:
			Write some text in $(_md README.md) that will present as inline $(bld code).

			Surround one or more words with $(bld backticks) $(_code '`').  A backtick is the quote
			mark that shares a key with the tilde $(_code '~').
			:
			;;

		$_BLOCK)
			cat <<-:
			Add a pre-formatted block (a.k.a. code block) to $(_md README.md).

			Start two lines with triple backticks $(_code '```') and insert some text in
			between those lines.  It is important that the backticks are all
			together, and all the way at the left margin (i.e., not indented).
			These special lines are called "fences".

			You can add the name of a programming language after the first fence,
			like this:

			$(_code '```python')

			If GitLab knows about that language, it will apply syntax highlighting
			to your code block.  If GitLab doesn't recognize that programming
			language, it will just show up as an ordinary pre-formatted block.
			:
			;;

		$_CHANGE)
			cat <<-:
			You need to change $(_md README.md) to proceed.
			Did you forget to save it in your editor?

			Add these $(_md) features to the file:
			  * Headings (both $(bld level 1) and $(bld level 2))
			  * $(bld Bold) text
			  * $(bld Bulleted list)
			  * $(bld Inline code) (not a code block)
			  * $(bld Pre-formatted) block (a.k.a. a code block)

			Refer back to $(_md instructions/Markdown.md) for syntax guidance.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO"
			;;

		$_README_MISSING)
			cat <<-:
			The fact that you deleted $(_md README.md) in the last lesson doesn't
			matter to $(cyn Nano).  Just run $(cyn Nano) the same way as if that file
			existed:
			  $(cmd nano README.md)

			$(_md README.md) will be re-created when you save and exit $(cyn Nano).
			:
			;;

		*)
			_tutr_generic_hint $1 nano "$_REPO"

			cat <<-:

			Edit $(path README.md) and add these $(_md) features:
			  * Headings (both $(bld level 1) and $(bld level 2))
			  * $(bld Bold) text
			  * $(bld Bulleted list)
			  * $(bld Inline code) (not a code block)
			  * $(bld Pre-formatted) block (a.k.a. a code block)

			Refer back to $(_md instructions/Markdown.md) for guidance on syntax.
			:
			;;
	esac
}

edit_readme_epilogue() {
	cat <<-:
	Sweet!

	:
	_tutr_pressanykey
}




# git add, git commit, git push
push_readme_rw() {
	git reset HEAD~
	git push -f origin master
}

push_readme_ff() {
	git commit -am "automatic commit - README.md now has Markdown"
	git push origin master
}

push_readme_pre() {
	_COMMIT=$(git rev-parse master)
}

push_readme_prologue() {
	cat <<-:
	$(cmd git add), $(cmd git commit), and $(cmd git push) this change to $(_md README.md).
	:
}

push_readme_test() {
	_UNSTAGED=99
	_STAGED=98
	_BRANCH_AHEAD=97
	_ENCOURAGEMENT=96
	_NEW_COMMIT=$(git rev-parse master)

	[[ "$PWD" != "$_REPO" ]] && return $WRONG_PWD
	_tutr_file_unstaged README.md && return $_UNSTAGED
	_tutr_file_staged   README.md && return $_STAGED
	_tutr_branch_ahead            && return $_BRANCH_AHEAD
	if ! _tutr_branch_ahead && [[ $_COMMIT != $_NEW_COMMIT ]]; then
		_COMMIT=$_NEW_COMMIT
		return 0
	fi
	return $_ENCOURAGEMENT
}

push_readme_hint() {
	case $1 in
		$_UNSTAGED)
			cat <<-:
			Prepare your changes for commit by running $(cmd git add README.md).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save your changes in the
			$(_Git) repository.
			:
			;;

		$_BRANCH_AHEAD)
			cat <<-:
			Now run $(cmd git push) to send your code up to the GitLab server.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO"
			;;

		*)
			cat <<-:
			The steps of your $(_Git) workflow are
			  0. $(cmd git add FILENAME)
			  1. $(cmd 'git commit -m "Brief commit message"')
			  2. $(cmd git push)

			Why don't you take a small step away from the keyboard and think about
			what you need to do to proceed in the lesson.
			:
			;;

	esac
}

push_readme_epilogue() {
	_tutr_pressanykey

	cat <<-':'

	Always watch for the receipt that is printed after you push:

	***********************************************************************
	*           __  ________  __  _____                ____    _          *
	*          / / / / __/ / / / / ___/__  __ _  ___  / __/___(_)         *
	*         / /_/ /\ \/ /_/ / / /__/ _ \/  ' \/ _ \_\ \/ __/ /          *
	*         \____/___/\____/  \___/\___/_/_/_/ .__/___/\__/_/           *
	*                                         /_/                         *
	*  ,/         \,                                                      *
	* ((__,-"""-,__))                                                     *
	*  `--)~   ~(--`                                                      *
	* .-'(       )'-,                                                     *
	* `--`d\   /b`--`  Big Blue says:                                     *
	*     |     |                                                         *
	*     (6___6)  Your submission arrived Sat 16 Jul 2022 18:39:59 MDT   *
	*      `---`                                                          *
	*                                                                     *
	***********************************************************************
	:

	cat <<-:

	If you $(bld "don't") see this receipt, there is a possibility that you have
	pushed your code to $(bld another Git server) (students have done this before).
	If I can't find your code on my sever, $(bld "I can't grade it").

	Assignments are due before $(bld midnight) as judged by the clock on $(bld this)
	GitLab server.  It doesn't matter if it is running faster than the clock
	on your laptop, your microwave, your phone, or the official atomic clock
	at the Naval Observatory.  $(bld This clock) is the $(bld sole arbiter) of lateness at
	$(ylw DuckieCorp).

	:
	_tutr_pressanykey
}



git_log0_prologue() {
	cat <<-:
	After you push, it is always a good idea to look at the repository over
	on GitLab to make sure that everything you expected to send actually
	arrived there.  If you get in this habit you can $(bld avoid a lot of stress)
	in your life, believe me!

	(If you have closed your browser, you can either run $(cmd browse_repo) or
	manually visit
	  $(path $_REPO_HTTPS)
	to see this repo in GitLab).

	Once you're on GitLab, I want you to compare the $(ylw_ commit ID) shown near
	the upper-left corner of the webpage with the $(ylw_ commit ID) shown at the top
	of the $(cmd git log).  (Remember, these are the big, long numbers that look
	like $(ylw_ b78e1a67282912936f9e6ca44bb1b4030681f6d0))

	:

	_tutr_pressanykey

	cat <<-:

	Here are some things to look for:

	* The first 7-8 characters of the first $(ylw_ commit ID) in the log should
	  match what's shown on GitLab.

	* If you can't see the top line of $(cmd git log), you have run into a quirk
	  of the $(cmd less) pager.  Press $(kbd G) followed by $(kbd g) to make it come back. 

	* All of the files that you have $(cmd git add)ed and $(cmd git commit)ted should look
	  the same on $(_remote GitLab) as they do here on $(_local your computer).

	So, run $(cmd git log) and make sure both repos are the same again.
	:
}

git_log0_test() {
	_tutr_generic_test -c git -a log -d "$_REPO"
}

git_log0_hint() {
	_tutr_generic_hint $1 "git log" "$_REPO"
}



# Navigate ../src, view plotter.py in nano and browser and assert `_tutr_file_clean plotter.py`
# Remark about line numbers and syntax highlighting in GitLab for
#   source code files.  GitLab just autodetects this based on filename
view_plotter_py_rw() {
	cd "$_REPO"
}

view_plotter_py_ff() {
	cd "$_REPO/src"
}

view_plotter_py_prologue() {
	cat <<-:
	It's time to get down to business.  There is a $(_py) program called 
	$(_py plotter.py) up in the $(path src/) directory.  Go into that directory and look at
	that program with the $(cmd less) file viewer.

	While you're looking at it over here, navigate to the same file on
	GitLab and have a look at it on the web.

	What differences can you see?
	:
}

view_plotter_py_test() {
	_CHANGED=99
	_STAGED=98

	[[ "$PWD" != "$_REPO/src" ]] && return $WRONG_PWD
	_tutr_file_unstaged src/plotter.py && return $_CHANGED
	_tutr_file_staged src/plotter.py && return $_STAGED

	case ${_CMD[0]} in
		nano|emacs|*vim|*vi|less|more|code|charm*|vscode)
			[[ ${_CMD[1]} == plotter.py ]] && return 0
			;;
	esac

	_tutr_generic_test -c less -a plotter.py -d "$_REPO/src"
}

view_plotter_py_hint() {
	case $1 in
		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/src"

			cat <<-:

			Then, when you get there, look at $(_py plotter.py) in the $(cmd less) viewer.
			:
			;;

		$_CHANGED)
			cat <<-:
			No, don't edit this file!

			You'd better put it back to the way it was before.  Undo your changes
			  $(cmd git restore plotter.py)
			and look again.  And this time $(bld look), and $(bld "don't touch")!
			:
			;;

		$_STAGED)
			cat <<-:
			Whoa, what are you trying to do?

			Now isn't the time for committing changes!  You are just supposed
			to be $(bld looking) at this file.  You'll change it later.

			Undo this with $(cmd git restore --staged plotter.py) and
			$(cmd git restore plotter.py).
			:
			;;
		*)
			_tutr_generic_hint $1 less "$_REPO/src"
			cat <<-:

			Have a look at the program on GitLab and with $(cmd less plotter.py).
			:
			;;
	esac
}

view_plotter_py_epilogue() {
	cat <<-:
	Hopefully there are $(bld NO) differences in the source code between here
	and GitLab!

	I do want point out that GitLab automatically detects this file's type
	and displays it with $(bld syntax highlighting).  It comes out like a
	pre-formatted block in $(_md), but without you doing anything besides
	giving it a name ending in $(_py .py).

	GitLab also draws $(bld line numbers) in the left margin.  They make it $(bld much)
	easier for you to direct others to a specific part of a program.  If you
	click on a line number, the browser updates the URL in the address bar
	to include it.  Then, if you give that address to another person (say, a
	TA when you have a question), they'll be taken right to that line and
	find it highlighted for them ($(bld hint, hint)).

	:
	_tutr_pressanykey
}



run_plotter_py0_prologue() {
	cat <<-:
	While you're here, you might as well run the program to see if it even
	works.

	  $(cmd $_PY plotter.py)
	:
}

run_plotter_py0_test() {
	_tutr_generic_test -c $_PY -a plotter.py -d "$_REPO/src"
}

run_plotter_py0_hint() {
	_tutr_generic_hint $1 $_PY "$_REPO"
}

run_plotter_py0_epilogue() {
	cat <<-:
	Remember the instructions for this program?  These plots sure don't look
	like the examples from that document!

	It looks like you've found a $(red bug)!

	Before you get all excited and try to fix it, $(cyn cool your jets).
	You're going to take it slow and do this the $(ylw DuckieCorp) way.

	:
	_tutr_pressanykey
}



edit_plan_md0_rw() {
	git reset HEAD~
	git push -f origin master
	_COMMIT=$(git rev-parse master)
}

edit_plan_md0_ff() {
	sed -i -e '/Phase 3: Testing/a\Made an edit for step edit_plan_md0' "$_REPO/doc/Plan.md"
	git commit -am "Automatic commit: phase 3 of doc/Plan.md"
	git push origin master
	_COMMIT=$(git rev-parse master)
}

edit_plan_md0_pre() {
	_COMMIT=$(git rev-parse master)
}

edit_plan_md0_prologue() {
	cat <<-:
	How many times have you "fixed" a $(red bug) in one of your programs, only to
	create a $(bld cascade) of other problems?  And then, several hours later and
	much to your dismay, you realize that you are in an $(bld even worse) place
	than you were at the beginning?

	I've been here, too, more times than I can remember.  I cannot begin to
	count the hours I've wasted this way.

	From my experience, this is how most professional software engineers go
	about their work.  As frustrating an experience this is for all of us,
	it amazes me that more programmers don't quit and start a new career!

	But it doesn't have to be like this for you.  You are still new enough
	to form $(bld better problem-solving) habits.  It will be hard at first, but I
	promise that it is $(bld worth it).

	:

	_tutr_pressanykey

	cat <<-:

	Before you start hacking on the code, pause and think $(bld carefully) about
	what you are trying to do.  Ask yourself these questions:

	  0. Do I really know what this code is $(bld supposed) to do?

	  1. Am I sure that this program isn't actually doing the right thing?
	     How do I know this?

	  2. What does the $(bld right thing) even look like?

	  3. Assuming I $(bld even know what the program ought to do), can I point to
	     the line (or lines) that are making it do the wrong thing?

	  4. Even if I could identify where the problem lies, am I really sure
	     what $(bld '"better"') code would look like?

	:

	_tutr_pressanykey

	cat <<-:

	Now, I know how obvious and stupid these questions appear.  Especially
	question #1.  But you skip them at your own peril.  I can't tell you $(bld how)
	$(bld many times) I had convinced myself that I found a $(red serious bug), only to
	learn later that the program was correct all along, and that the problem
	was with my assumptions.

	Then I got to go back and $(red undo) the precious fix I spent days writing.

	I've since learned that writing code isn't something that I can do by
	instinct.  It requires $(cyn careful thought), not a $(ylw feeling in my gut).  I am
	not able to get it right if I don't stop and ponder.  Answering these
	questions helps me to do that.

	:

	_tutr_pressanykey

	cat <<-:

	Before you attempt to "fix" this program, I want you to stop and write a
	little bit about it in the software development $(_md Plan.md).

	Write in your own words what the program $(bld actually did), and what you
	think it $(bld should have done) instead.  If you struggle to put these
	thoughts into plain English, how do you expect to explain it to the
	computer in a programming language?

	So, return to $(path ../doc) and edit $(_md Plan.md).

	Put your words under the section titled $(bld "Phase 3: Testing & Debugging").

	Finally, $(cmd git add), $(cmd git commit) and $(cmd git push) your remarks.
	:
}

edit_plan_md0_test() {
	_CLEAN=95
	_UNSTAGED=99
	_STAGED=98
	_BRANCH_AHEAD=97
	_ENCOURAGEMENT=96
	_NEW_COMMIT=$(git rev-parse master)

	[[ "$PWD" != "$_REPO/doc" ]] && return $WRONG_PWD
	if ! _tutr_branch_ahead && [[ $_COMMIT != $_NEW_COMMIT ]]; then
		_COMMIT=$_NEW_COMMIT
		return 0
	fi
	! _tutr_branch_ahead && _tutr_file_clean doc/Plan.md && return $_CLEAN
	_tutr_file_unstaged doc/Plan.md && return $_UNSTAGED
	_tutr_file_staged   doc/Plan.md && return $_STAGED
	_tutr_branch_ahead            && return $_BRANCH_AHEAD
	return $_ENCOURAGEMENT
}

edit_plan_md0_hint() {
	case $1 in
		$_CLEAN)
			cat <<-:
			Good, you're here! 

			Now edit $(_md Plan.md).  Write your thoughts under the section titled
			$(bld "Phase 3: Testing & Debugging").
			:
			;;

		$_UNSTAGED)
			cat <<-:
			Prepare your changes for commit by running $(cmd git add Plan.md).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save your changes in the
			$(_Git) repository.
			:
			;;

		$_BRANCH_AHEAD)
			cat <<-:
			Now run $(cmd git push) to send your code up to the GitLab server.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/doc"
			;;

		*)
			cat <<-:
			The steps of your $(_Git) workflow are
			  0. $(cmd git add FILENAME)
			  1. $(cmd 'git commit -m "Brief commit message"')
			  2. $(cmd git push)

			Why don't you take a small step away from the keyboard and think about
			what you need to do to proceed in the lesson.
			:
			;;
	esac
}

edit_plan_md0_epilogue() {
	_tutr_pressanykey

	cat <<-:

	You probably think I'm asking you to slow to a crawl.
	But you don't yet know what $(ylw fast) really is.

	Once you stop wasting hours upon hours chasing your tail, you will.

	:

	_tutr_pressanykey
}



# navigate to ../src;  edit plotter.py & assert `_tutr_file_unstaged plotter.py`
fix_plotter_py_prologue() {
	cat <<-:
	Do you feel clear about what needs to happen?

	Probably not, because you haven't really spent any time with this
	program.  You have only a vague notion about what it does and how it
	might work.  In real life, this is the part of the process where you
	would spend the most time.  You would read and re-read the program,
	while running it and observing where interesting things happen.

	When I began coding, I wasn't aware of how much time I would spend
	reading.  Reading code, reading books, reading websites; it adds up to a
	significant part of your workday.  Programmers are $(bld always) reading.  The
	part where you actually write code... isn't a very big part of your day.

	Anyway, we'll talk about this later.  I'll just give you the answer to
	this problem so you can get a move on.

	This program only prints asterisks, but never prints spaces!
	Look at the part of the program that begins on line 16:

	  $(_code "if l == line:")
	      $(_code "print('*', end='')")

	Instead of doing $(bld nothing) when $(_code "l != line"), it should print a $(bld space).

	Add these two lines right below that $(_code if) statement in $(_py plotter.py):
	  $(_code "else:")
	      $(_code "print(' ', end='')")

	Be sure to match the $(bld indentation) with its surroundings.  Use $(bld 4) spaces
	per level of indentation, and not tabs ($(_py) just $(red hates) tabs).
	:
}

fix_plotter_py_test() {
	[[ "$PWD" != "$_REPO/src" ]] && return $WRONG_PWD
	_tutr_file_unstaged src/plotter.py && return 0
	_tutr_generic_test -c nano -a plotter.py -d "$_REPO/src"
}

fix_plotter_py_hint() {
	case $1 in
		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/src" ;;
		*)
			_tutr_generic_hint $1 nano "$_REPO"
			cat <<-:

			Find this part of $(_py plotter.py), beginning on line 16:
			  $(_code "if l == line:")
			      $(_code "print('*', end='')")

			Add these two lines right below the $(_code if) statement:
			  $(_code "else:")
			      $(_code "print(' ', end='')")

			* Be sure to match the $(bld indentation) with its surroundings
			* Use $(bld 4) spaces per level of indentation
			* $(bld Do not) use tabs
			:
	esac
}

fix_plotter_py_epilogue() {
	cat <<-:
	Do you think that will work?  There is only one way to find out!

	:
	_tutr_pressanykey
}



# Inspect ${_CMD[@]}
run_plotter_py1_prologue() {
	cat <<-:
	Run $(_py plotter.py) again to test your fix.
	:
}

run_plotter_py1_test() {
	_tutr_generic_test -c $_PY -a plotter.py -d "$_REPO/src"
}

run_plotter_py1_hint() {
	_tutr_generic_hint $1 $_PY "$_REPO"
	cat <<-:

	Run $(_py plotter.py) again to test your fix.
	:
}

run_plotter_py1_epilogue() {
	cat <<-:
	Are you satisfied with that change?  Then commit it to $(_Git).

	:
	_tutr_pressanykey
}



# git add, git commit; assert `_tutr_file_clean` and `_tutr_branch_ahead`
# DON'T push it yet...
commit_plotter_py_ff() {
	cd "$_REPO/src"
	sed -i -e "17a\            else:\n                print(' ', end='')\n" "$_REPO/src/plotter.py"
	git add plotter.py
	git commit -m "Automatic commit: fixed src/plotter.py"
}

commit_plotter_py_rw() {
	git reset HEAD~
}

commit_plotter_py_prologue() {
	cat <<-:
	$(cmd git add) and $(cmd 'git commit -m "..."') to record this fix.

	Don't forget to wrap your commit message in $(kbd quote marks)!

	Hold off on pushing the commit for now.
	:
}

commit_plotter_py_test() {
	_UNSTAGED=99
	_STAGED=98
	_ENCOURAGEMENT=96

	[[ "$PWD" != "$_REPO/src" ]]       && return $WRONG_PWD
	_tutr_file_unstaged src/plotter.py && return $_UNSTAGED
	_tutr_file_staged   src/plotter.py && return $_STAGED
	_tutr_branch_ahead                 && return 0
	return $_ENCOURAGEMENT
}

commit_plotter_py_hint() {
	case $1 in
		$_UNSTAGED)
			cat <<-:
			Prepare your changes for commit by running $(cmd git add plotter.py).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save your changes in the
			$(_Git) repository.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/src"
			;;

		*)
			cat <<-:
			$(cmd git add) and $(cmd 'git commit -m "..."') to record this fix.

			Don't forget to wrap your commit message in $(kbd quote marks)!

			Hold off on pushing the commit for now.
			:
			;;
	esac
}

commit_plotter_py_epilogue() {
	_tutr_pressanykey
}



# navigate to ../doc; edit Plan.md; git add, git commit: assert `_tutr_file_clean` and `_tutr_branch_ahead`
# DON'T push it yet
# Amend comment in phase 3
# Add a note under phase 2 that some bugs were found and fixed during testing.
edit_plan_md1_ff() {
	cd "$_REPO/doc"
	echo "Automatic edit: phase 2 & phase 3, something something, fixed a bug" >> Plan.md
	git add Plan.md
	git commit -m "Automatic commit: Updated doc/Plan.md"
}

edit_plan_md1_rw() {
	cd "$_REPO/doc"
	git reset HEAD~
	git restore Plan.md
}

edit_plan_md1_pre() {
	_COMMIT=$(git rev-parse master)
}

edit_plan_md1_prologue() {
	cat <<-:
	The moment after making a code change is another opportunity for thought
	and reflection.  Return to $(path ../doc) and write a note in $(_md Plan.md) about
	$(bld what) you changed and, more importantly, $(bld why) you changed it.  Use plain
	language to explain why this was a good idea.

	There are two places in $(_md Plan.md) that you should update:

	0. $(bld "Phase 3: Testing & Debugging")
	   Add a line or two to your previous comment that details what the
	   problem actually was, and how you addressed it.

	1. $(bld "Phase 2: Implementation")
	   Mention that, in the course of testing, a problem came up which was
	   overlooked when the program was first created.
	
	$(cmd git add) and $(cmd git commit) this change, but don't push yet.
	:
}

edit_plan_md1_test() {
	_CLEAN=95
	_UNSTAGED=99
	_STAGED=98
	_ENCOURAGEMENT=96
	_NEW_COMMIT=$(git rev-parse master)

	[[ "$PWD" != "$_REPO/doc" ]] && return $WRONG_PWD
	_tutr_file_clean doc/Plan.md && [[ $_COMMIT == $_NEW_COMMIT ]] && return $_CLEAN
	_tutr_file_unstaged doc/Plan.md && return $_UNSTAGED
	_tutr_file_staged   doc/Plan.md && return $_STAGED

	_tutr_branch_ahead_count
	local count=$?
	(( count >= 2 )) && return 0

	return $_ENCOURAGEMENT
}

edit_plan_md1_hint() {
	case $1 in
		$_CLEAN)
			cat <<-:
			Edit $(_md Plan.md) and add a note about $(bld what) you changed and, more
			importantly, $(bld why) you changed it.

			There are two places in $(_md Plan.md) that you should update:

			0. $(bld "Phase 3: Testing & Debugging")
			1. $(bld "Phase 2: Implementation")
			:
			;;

		$_UNSTAGED)
			cat <<-:
			Prepare your changes for commit by running $(cmd git add Plan.md).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save your changes in the
			$(_Git) repository.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/doc"
			;;

		*)
			cat <<-:
			$(cmd git add) and $(cmd 'git commit -m "..."') to record this fix.

			Don't forget to wrap your commit message in $(kbd quote marks)!

			Hold off on pushing the commit for now.
			:
			;;
	esac
}

edit_plan_md1_epilogue() {
	cat <<-:
	There are many reasons why complete, and well-written documentation is
	valued so highly at $(ylw DuckieCorp).  An aphorism I keep going back to is:

		  $(cyn '	"The only thing worse than no documentation ')
		  $(cyn '			is obsolete documentation."         ')

	A little time devoted to keeping up documentation does wonders for the
	culture of a company.  Not to mention the sanity of its developers!

	:
	_tutr_pressanykey
}



edit_signature_md_ff() {
	cd "$_REPO/doc"
	echo "Automatic edit: Signature.md" >> Signature.md
	git add Signature.md
	git commit -m "Automatic commit: Updated doc/Signature.md"
}

edit_signature_md_rw() {
	cd "$_REPO/doc"
	git reset HEAD~
	git restore Signature.md
}

edit_signature_md_pre() {
	_COMMIT=$(git rev-parse master)
}

edit_signature_md_prologue() {
	cat <<-:
	There!  The program is working and the documentation is up-to-date.
	It's time to call it a day!

	There is one last thing to write before you clock out.  Jot down a
	summary of your day's efforts in the Sprint $(_md Signature.md).

	This file helps you to recognize when you are $(bld getting stuck) on a
	project.  If you write the same summary for a few days in a row, that's
	a $(bld sure sign) that you are not making real progress.  That is a good time
	to reach out for help.  Again, in my career I've wasted countless hours
	spinning my wheels and fooling myself into believing I was getting
	somewhere.

	The little bit of personal accountability and self-reflection the
	Signature affords will save days and weeks of frustration.

	Right now, open $(_md Signature.md) in $(_code Nano), clear out the placeholder text,
	and make an entry for today.

	$(cmd git add) and $(cmd 'git commit -m "..."') the file, but don't push yet
	(it's coming up soon, I promise)!
	:
}

edit_signature_md_test() {
	_CLEAN=95
	_UNSTAGED=99
	_STAGED=98
	_ENCOURAGEMENT=96
	_NEW_COMMIT=$(git rev-parse master)

	[[ "$PWD" != "$_REPO/doc" ]]    && return $WRONG_PWD
	_tutr_file_clean doc/Signature.md && [[ $_COMMIT == $_NEW_COMMIT ]] && return $_CLEAN
	_tutr_file_unstaged doc/Signature.md && return $_UNSTAGED
	_tutr_file_staged   doc/Signature.md && return $_STAGED

	_tutr_branch_ahead_count
	local count=$?
	(( count >= 3 )) && return 0

	return $_ENCOURAGEMENT
}

edit_signature_md_hint() {
	case $1 in
		$_CLEAN)
			cat <<-:
			Edit $(_md Signature.md), clear out the placeholder text, and make an
			entry describing your work today.
			:
			;;
		$_UNSTAGED)
			cat <<-:
			Prepare your changes for commit by running $(cmd git add Signature.md).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save your changes in the
			$(_Git) repository.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/doc"
			;;

		*)
			cat <<-:
			$(cmd git add) and $(cmd 'git commit -m "..."') to record this fix.

			Don't forget to wrap your commit message in $(kbd quote marks)!

			You'll get to $(cmd git push) this soon, just hold tight!
			:
			;;
	esac
}




git_status_prologue() {
	cat <<-:
	You've amassed a quite a few commits today.  Before you push them to
	GitLab, I want you to see what divergent repositories look like.  Some
	students forget to push their work, and lose points for their incomplete
	submissions.

	Remember, we $(bld "don't grade") the code you have on your computer; we can only
	grade what we find on GitLab. Learn to recognize when your repository
	has commits that aren't yet on GitLab.

	The first thing to look at is $(cmd git status).
	:
}

git_status_test() {
	_tutr_generic_test -c git -a status -d "$_REPO/doc"
}

git_status_hint() {
	_tutr_generic_hint $1 git "$_REPO/doc"
	cat <<-:
	
	Run $(cmd git status) to continue.
	:
}

git_status_epilogue() {
	_tutr_pressanykey
	cat <<-:

	Among $(cmd git status)'s output you will see this message:
	  $(_code "Your branch is ahead of 'origin/master' by 3 commits.")

	This is the first $(red red flag) telling you that you haven't submitted your
	work to GitLab.  Your repository has 3 commits that the $(_origin) does not have.

	:
	_tutr_pressanykey
}




# Inspect ${_CMD[@]}
# note that HEAD -> master is many commits ahead of (origin/master)
# Compare the local log with GitLab's
git_log1_prologue() {
	cat <<-:
	Now take a look at the log.  This will show you exactly which commits
	are missing from GitLab.  Notice that $(cyn "HEAD ->") $(grn master) is not on the
	same commit as $(red origin/master).  That is the clue that you're looking for.

	At the same time, hop on to the browser and look at your repo on GitLab.
	Compare the $(ylw_ Commit ID) in the upper-left corner with the one you
	see in $(cmd git log).

	(If you closed that tab, get it back with $(cmd browse_repo))
	:
}

git_log1_test() {
	_tutr_generic_test -c git -a log -d "$_REPO/doc"
}

git_log1_hint() {
	_tutr_generic_hint $1 git "$_REPO/doc"
}

git_log1_epilogue() {
	cat <<-:
	I've given you three ways to make sure that your complete project is submitted:
	  0. $(cmd git status)
	  1. $(cmd git log)
	  2. Look at your repository on GitLab

	Now, this only works if you $(bld "don't procrastinate")!  If you are working
	right up to midnight, you don't leave yourself enough time to $(bld double) and
	$(ylw triple-check) that all of your code is on GitLab.  And, if you find that
	something is missing at 11:59 pm (say, you forgot to $(cmd git add) a new
	file), you don't have enough time to fix it!

	:
	_tutr_pressanykey
}




# ! _tutr_branch_ahead, ${_CMD[@]} = 'git log'*
# Compare the local log with GitLab's again
big_push_ff() {
	git push
}

big_push_prologue() {
	cat <<-:
	Finally, the moment you've been waiting for!  $(cmd git push) your commits up
	to GitLab so your work can be graded.
	:
}

big_push_test() {
	_tutr_generic_test -c git -a push -d "$_REPO/doc"
}

big_push_hint() {
	_tutr_generic_hint $1 git "$_REPO/doc"
	cat <<-:

	Run $(cmd git push) to proceed
	:
}

big_push_epilogue() {
	_tutr_pressanykey
	cat <<-:

	You might want to know why I don't suggest that you just $(bld always push)
	immediately after $(bld every) commit.  For now, while you are still a
	beginner, $(bld always) pushing after $(bld every) commit isn't a bad thing to do.

	However, there are situations where advanced $(_Git) users prefer to work in
	a repository that is isolated from the remote.  The great thing about $(_Git) is
	that it supports both styles of workflow!

	:
	_tutr_pressanykey

	cat <<-:

	Whenever you push to my GitLab server, $(bld always) check for the push receipt
	that shows $(blu Big Blue) and the arrival time.

	This is also a good opportunity to refresh your browser and make sure
	that your latest commit arrived, as expected.

	(If you closed that tab, get it back with $(cmd browse_repo))
	:
}



make_certificate_pre() {
	_tutr_record_completion ${_TUTR#./}
}

make_certificate_prologue() {
	cat <<-:
	You've made it to the end of the Shell Tutorial.  The last thing for you
	to do is create a $(ylw Certificate of Completion) and push it to GitLab.

	Go back into the Shell Tutor directory that you started from and run
	$(cmd ./make-certificate.sh).  This will create a file that you will
	move back into this repository's $(path doc) directory.

	Don't worry, I'll walk you through the whole process  ;)
	:

	_tutr_shortest_path "$_BASE" "$PWD"
	if [[ -n "$REPLY" ]]; then
		cat <<-:

		You can start by $(cmd cd)'ing to $(path $REPLY)
		:
	fi
}

make_certificate_test() {
	[[ "$PWD" != "$_BASE" ]] && return $WRONG_PWD
	_tutr_file_ignored certificate.txt && return 0
	_tutr_generic_test -c ./make-certificate.sh -d "$_BASE"
}

make_certificate_hint() {
	case $1 in
		$WRONG_PWD) _tutr_minimal_chdir_hint "$_BASE" ;;
		*) _tutr_generic_hint $1 ./make-certificate.sh "$_BASE" ;;
	esac
}

make_certificate_epilogue() {
	cat <<-:
	It isn't much to look at, but I suppose you can print it out
	and hang it on your fridge.

	:
	_tutr_pressanykey
}





# PWD in _BASE && _tutr_file_untracked "$_REPO/doc/certificate.txt"
# ${PWD}* = _BASE
mv_cert_prologue() {
	_tutr_shortest_path "$_REPO/doc" "$PWD"

	cat <<-:
	As suggested, move $(path certificate.txt) from here into the $(path doc/) directory of
	your Assignment #0 repository (i.e. $(path $REPLY))
	:
}

mv_cert_test() {
	_NOT_MOVED=99
	[[ "$PWD" != "$_BASE" ]] && return $WRONG_PWD
	[[ -f "$_REPO/doc/certificate.txt" ]] && return 0
	_tutr_file_ignored certificate.txt && return $_NOT_MOVED
	_tutr_generic_test -c mv -a certificate.txt -a ../cs1440-assn0/doc -d "$_BASE"
}

mv_cert_hint() {
	case $1 in
		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_BASE"
			;;

		$_NOT_MOVED)
			cat <<-:
			What are you waiting for?

			Move it with $(cmd mv certificate.txt ../cs1440-assn0/doc).
			:
			;;

		*)
			_tutr_generic_hint $1 mv "$_BASE"
			cat <<-:

			You need to $(cmd mv certificate.txt ../cs1440-assn0/doc).
			:
			;;
	esac
}




push_cert_prologue() {
	cat <<-:
	Return to $(path ../cs1440-assn0/doc), where you can $(cmd git add), $(cmd git commit), and
	$(cmd git push) the certificate.
	:
}

push_cert_test() {
	_UNTRACKED=99
	_STAGED=98
	_BRANCH_AHEAD=97
	_ENCOURAGEMENT=96
	_MISSING=95
	[[ "$PWD" != "$_REPO/doc" ]] && return $WRONG_PWD
	[[ ! -f "$_REPO/doc/certificate.txt" ]] && return $_MISSING
	_tutr_file_untracked doc/certificate.txt && return $_UNTRACKED
	_tutr_file_staged doc/certificate.txt && return $_STAGED
	_tutr_branch_ahead && return $_BRANCH_AHEAD
	return 0
}

push_cert_hint() {
	case $1 in
		$_UNTRACKED)
			cat <<-:
			Add $(path certificate.txt) to the next commit with $(cmd git add certificate.txt).
			:
			;;

		$_STAGED)
			cat <<-:
			Run $(cmd 'git commit -m "..."') to permanently save the certificate to the
			$(_Git) repository.
			:
			;;

		$_BRANCH_AHEAD)
			cat <<-:
			Now run $(cmd git push) to submit the certificate to GitLab.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_REPO/doc"
			;;

		$_MISSING)
			cat <<-:
			Uh-oh!  Where did $(path certificate.txt) go?  You'll have to track it
			down and replace it in the $(path doc/) directory before you can finish.

			If all else fails, you can go back to the shell tutor directory
			and re-create it.
			:
			;;

		*)
			cat <<-:
			The steps of your $(_Git) workflow are
			  0. $(cmd git add certificate.txt)
			  1. $(cmd 'git commit -m "Brief commit message"')
			  2. $(cmd git push)

			Why don't you take a small step away from the keyboard and think about
			what you need to do to proceed in the lesson.
			:
			;;

	esac
}

push_cert_epilogue() {
	cat <<-:
	You weren't going to neglect making sure that file made it to GitLab,
	right?

	Refresh your browser and ensure $(path certificate.txt) is there.
	(If you closed that tab, get it back with $(cmd browse_repo))
	:
}


epilogue() {
	cat <<-EPILOGUE
	You've been fantastic!

	You are all ready to have a successful internship at $(ylw DuckieCorp).
	                                                              ${_Y}     _
	                                                              ${_Y}    ( |
	${_C} _____ _         _   _           _ _      __     _ _       _  ${_Y}  ___\\ \\
	${_C}|_   _| |_  __ _| |_( )___  __ _| | |    / _|___| | |__ __| | ${_Y} (__()  \`-|
	${_C}  | | | ' \\/ _\` |  _|/(_-< / _\` | | |_  |  _/ _ \\ | / /(_-<_| ${_Y} (___()   |
	${_C}  |_| |_||_\\__,_|\\__| /__/ \\__,_|_|_( ) |_| \\___/_|_\\_\\/__(_) ${_Y} (__()    |
	${_C}                                    |/                        ${_Y} (_()__.--|
	EPILOGUE
}




source main.sh  && _tutr_begin \
	rename_repo \
	cd_repo \
	ls0 \
	view_instructions \
	view_markdown_md \
	view_plan \
	edit_readme \
	push_readme \
	git_log0 \
	view_plotter_py \
	run_plotter_py0 \
	edit_plan_md0 \
	fix_plotter_py \
	run_plotter_py1 \
	commit_plotter_py \
	edit_plan_md1 \
	edit_signature_md \
	git_status \
	git_log1 \
	big_push \
	make_certificate \
	mv_cert \
	push_cert

# vim: set filetype=sh noexpandtab tabstop=4 shiftwidth=4 textwidth=76 colorcolumn=76 formatoptions=qron:
