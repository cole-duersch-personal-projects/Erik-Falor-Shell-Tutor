#!/bin/sh

_HELP="Lesson #2 Topics
================
* Make copies of files
* Move and rename files
* Remove files
* Refer to many files at once
* Take advantage of tab completion

Commands used in this lesson
============================
* cp
* mv
* rm
"

# Put tutorial library files into $PATH
PATH=$PWD/.lib:$PATH

. shell-compat-test.sh

source record.sh
source ansi-terminal-ctl.sh
if [[ -n $_TUTR ]]; then
	source generic-error.sh
	source nonce.sh
	source platform.sh
fi

create_copy0_txt() {
	cat <<-TEXT > "$_BASE/copy0.txt"
	Use the copy command 'cp' to make two copies of this file.
	When you're done you will have three files:
	* copy0.txt
	* copy1.txt
	* copy2.txt
	TEXT
}

create_move0_txt() {
	cat <<-TEXT > "$_BASE/move0.txt"
	Use the move command 'mv' to rename this file.
	When you're done you will only have this one file, but it won't be
	named move0.txt anymore.
	TEXT
}

create_different_txt() {
	cat <<-TEXT > "$_BASE/different.txt"
	This file is not like the others!
	TEXT
}

# Find the command with the longest name on this system
# Set $REPLY to this command's name
find_longest_command() {
	if [[ -n $ZSH_NAME ]]; then
		setopt local_options nullglob
	fi
	local LONGEST=
	for CMD in /bin/* /usr/bin/* /usr/local/bin/*; do
		if [[ -x $CMD ]]; then
			CMD=${CMD##*/}
			if [[ ${#CMD} -gt ${#LONGEST} ]]; then
				LONGEST=$CMD
			fi
		fi
	done
	REPLY=$LONGEST
}

setup() {
	if _tutr_record_exists ${_TUTR#./}; then
		_tutr_warn printf "'You have already completed this lesson'"
		if ! _tutr_yesno "Would you like to do it again?"; then
			_tutr_info printf "'SEE YOU SPACE COWBOY...'"
			exit 1
		fi
	fi

	source screen-size.sh 80 30
	export _HERE="$PWD"
	export _BASE="$PWD/lesson2"
	[[ -d $_BASE ]] && rm -rf "$_BASE"
	mkdir -p "$_BASE"

	create_copy0_txt
	create_move0_txt
	create_different_txt

	find_longest_command
	export _LONGEST=$REPLY
}


prologue() {
	[[ -z $DEBUG ]] && clear
	echo
	cat <<-PROLOGUE
	Shell Lesson #2: Manipulating Files

	In this lesson you will learn to

	* Copy files
	* Move and rename files
	* Remove files
	* Refer to multiple files with wildcards

	Let's get started!

	PROLOGUE

	_tutr_pressanykey
}


cleanup() {
	# Remember that this lesson has been completed
	(( $# >= 1 && $1 == $_COMPLETE)) && _tutr_record_completion ${_TUTR#./}
	[[ -d $_BASE ]] && rm -rf "$_BASE"
	echo "You worked on Lesson #2 for $(_tutr_pretty_time)"
}


epilogue() {
	cat <<-EPILOGUE
	That wraps things up for now.  Before long the people looking over your
	shoulder will ask what you're hacking into and when the FBI will get
	here.  On that day you will be a true shell user.

	In this lesson you learned how to

	* Copy files
	* Move and rename files
	* Remove files
	* Refer to multiple files with wildcards

	This concludes Shell Lesson #2

	Run $(cmd ./3.0-directories.sh) to enter the next lesson

	EPILOGUE

	_tutr_pressanykey
}


# Case- and whitespace-insensitively compare a one line file with a specimen string
# Return 0 if the file contains only the specimen
# Return 1 otherwise
compare() {
	if (( $# < 2 )); then
		cat <<-:
		Usage:
		compare filename "string"
		:
		return 1
	fi

	eval 'tr A-Z a-z < $1 | tr -s "[[:blank:]]" | cmp - <(echo $2) >/dev/null 2>/dev/null'
}


copy01_rw() {
	command rm -f "$_BASE/copy1.txt"
}

copy01_ff() {
	create_copy0_txt
	command cp -f "$_BASE/copy0.txt" "$_BASE/copy1.txt"
}

copy01_prologue() {
	cat <<-:
	Do you remember when I told you that shell commands are short and easy
	to type?  I hope that you aren't too attached to vowels...

	The Unix file copy command is named $(cmd cp).  It makes a copy of a file
	SOURCE named DESTINATION:

	  $(cmd cp SOURCE DESTINATION)

	Example: to make a copy of a file $(path hello.txt) named $(path world.doc)
	  $(cmd cp hello.txt world.doc)

	There are a few files here; see for yourself with $(cmd ls).
	Use $(cmd cp) to copy $(path copy0.txt) to $(path copy1.txt)
	:
}

copy01_test() {
	if   _tutr_nonce; then return $PASS
	elif [[ -f $_BASE/copy0.txt && -f $_BASE/copy1.txt ]]; then return 0
	elif [[ ! -f $_BASE/copy0.txt ]]; then return 99
	else _tutr_generic_test -c cp -a copy0.txt -a copy1.txt -d "$_BASE"
	fi
}

copy01_hint() {
	case $1 in
		99)
			create_copy0_txt

			cat <<-:
			What happened to $(path copy0.txt)?
			No worries, I've replaced it for you.  Please try again.
			:
			;;
		*)
			_tutr_generic_hint $1 cp "$_BASE"
			;;
	esac

	cat <<-:

	Use $(cmd cp) to make a copy of $(path copy0.txt) called $(path copy1.txt).
	Your command should look like this:
	  $(cmd cp copy0.txt copy1.txt)
	:
}



copy02_rw() {
	command rm -f "$_BASE/copy2.txt"
}

copy02_ff() {
	create_copy0_txt
	command cp -f "$_BASE/copy0.txt" "$_BASE/copy2.txt"
}

copy02_prologue() {
	cat <<-:
	Use $(cmd cp) again to make another copy of $(path copy0.txt) called $(path copy2.txt).

	:
}

copy02_test() {
	if   _tutr_nonce; then return $PASS
	elif [[ -f $_BASE/copy0.txt && -f $_BASE/copy2.txt ]]; then return 0
	elif [[ ! -f $_BASE/copy0.txt ]]; then return 99
	else _tutr_generic_test -c cp -a copy0.txt -a copy2.txt -d "$_BASE"
	fi
}

copy02_hint() {
	case $1 in
		99)
			create_copy0_txt

			cat <<-:
			What happened to $(path copy0.txt)?
			Never mind, I've already replaced it for you.  Please try again.
			:
			;;
		*)
			_tutr_generic_hint $1 cp "$_BASE"
			;;
	esac

	cat <<-:

	Use $(cmd cp) to make a copy of $(path copy0.txt) called $(path copy2.txt).
	Your command should look like this:
	  $(cmd cp copy0.txt copy2.txt)
	:
}


list1_prologue() {
	cat <<-:
	See the result of your work with $(cmd ls)
	:
}

list1_test() {
	_tutr_generic_test -c ls -d "$_BASE"
}

list1_hint() {
	_tutr_generic_hint $1 ls "$_BASE"
}

list1_epilogue() {
	_tutr_pressanykey
	cat <<-:

	It's a good idea to frequently use $(cmd ls) to make sure that your files are
	just as you expect.  Caution pays off as there is no 'undo' button in
	the shell.

	:
	_tutr_pressanykey
}


cp_clobber_ff() {
	_PREV="cp copy0.txt different.txt"
}

cp_clobber_pre() {
	[[ ! -f $_BASE/copy0.txt ]] && create_copy0_txt
}

cp_clobber_prologue() {
	cat <<-:
	To recap, you have made two copies of $(path copy0.txt):
	*   $(path copy1.txt)
	*   $(path copy2.txt)
	All three files are identical.

	When you ran $(cmd cp) there weren't already files named $(path copy1.txt) and
	$(path copy2.txt).  If those files had already existed they would have been
	overwritten, permanently losing their original contents.

	As a precaution the $(cmd cp) command $(bld MAY) prompt you for confirmation before
	overwriting a file.  Or, it might $(bld SILENTLY overwrite) the destination
	file with the source.  It depends on how your shell is set up on your
	computer.

	You should know which way $(cmd cp) behaves for you.

	Copy $(path copy0.txt) onto $(path different.txt) to find out if $(cmd cp) stops and asks
	you to proceed.

	If $(cmd cp) does ask for permission, just hit $(kbd ENTER).
	:
}

cp_clobber_test() {
	_tutr_generic_test -i -c cp -a copy0.txt -a different.txt -d "$_BASE"
}

cp_clobber_hint() {
	_tutr_generic_hint $1 cp "$_BASE"

	cat <<-:
	Copy $(path copy0.txt) onto $(path different.txt) to see whether $(cmd cp) stops and asks
	you to proceed.

	The command you need to run is
	  $(cmd cp copy0.txt different.txt)

	If $(cmd cp) asks for permission, just hit $(kbd ENTER).
	:
}

cp_clobber_epilogue() {
	_tutr_pressanykey

	cat <<-:

	That was enlightening!

	When $(cmd cp) asks for confirmation you may respond in the affirmative with
	any string that begins with "$(bld Y)" or "$(bld y)".  Each of these strings are
	understood as affirmative responses:

	*   "$(bld YES)"
	*   "$(bld yup)"
	*   "$(bld yippee)"
	*   "$(bld "Yancy yearns for yesterday's yarrow yogurt")"
	
	For the sake of brevity, you can just enter "$(kbd y)".

	Any other string (including $(kbd ENTER) by itself) is a negative response.

	Once a file is overwritten there is no EASY way to get it back.  In the
	last lesson of this tutorial I'll teach you about a program that can
	help with this, but caution is always the best policy.
	:
	_tutr_pressanykey
}

cp_clobber_post() {
	_PREV=${_CMD[@]}
}



tabcmplt_ls_d_prologue() {
	cat <<-:
	After running all of those commands, I'll bet that you feel like you are
	doing WAY too much typing.  The last command you ran,
	  $(cmd $_PREV)
	was $(bld ${#_PREV}) characters long.  That's just too much typing for a lazy
	Unix hacker!

	Your phone and web browser are able to figure out what you want to say
	and finish your words for you.  Your word processor can do this, too.
	Even your programming environment has code completion.  These 21st
	century innovations are great time-savers.

	Did I say 21st century?  I meant 20th century.
	Command shells have been doing this since the early 90's.

	:

	_tutr_pressanykey

	cat <<-:

	From now on, all you'll ever need to type are first few characters of a
	file's name, followed by $(kbd TAB).  If ${_SH} can find a file with a name
	beginning with those characters, it will finish typing it for you.

	Try it now with the $(cmd ls) command.  I want you to list ONLY the file
	$(path different.txt).

	The command to run, $(cmd ls different.txt), is 16 characters long.

	Instead, I want you to type this:
	  $(cmd ls d)$(kbd '<TAB>')
	and see ${_SH} auto-magically fill in the rest (obviously, you will press
	the $(kbd Tab key) instead of typing out $(kbd '<TAB>')).

	Then, run that command to pass this step.

	:

	_tutr_pressanykey
}

tabcmplt_ls_d_test() {
	_tutr_generic_test -c ls -a different.txt -d "$_BASE" 
}

tabcmplt_ls_d_hint() {
	_tutr_generic_hint $1 ls "$_BASE"

	cat <<-:

	Type $(cmd ls d)$(kbd '<TAB>') so you can more easily run $(cmd ls different.txt)
	:
}

tabcmplt_ls_d_epilogue() {
	cat <<-:
	That's like magic, isn't it?

	:
	_tutr_pressanykey
}



tabcmplt_ls_c_prologue() {
	cat <<-:
	That last example worked so well because there is only one filename
	starting with '$(bld d)'.  What happens when there are other possibilities?
	It all depends on which shell you are running and how it is configured.

	You've got 3 files here with names that begin with '$(bld copy)'.
	Let's consider what happens when you type
	  $(cmd ls c)$(kbd '<TAB>')

	:

	_tutr_pressanykey

	if [[ -n $ZSH_NAME ]]; then
		cat <<-:

		You are using Zsh, which has lots of options for configuring how Tab
		completion works.  Actually, Zsh just has a lot of options in general,
		but that's a rabbit hole you can explore on your own time.

		Unless you have already customized your Zsh, I can make a decent guess
		about what you will see when you try this.

		:

		_tutr_pressanykey

		cat <<-:

		All 3 of the '$(bld c)' files' names have the first 4 letters in common.  When
		you first press $(kbd '<TAB>'), Zsh fills in as much as is common among those
		files, so '$(bld c)' becomes '$(bld copy)'.  Press $(kbd '<TAB>') again, and the remaining
		possibilities are displayed below your command line.

		Continue to press $(kbd '<TAB>'), and the command line will change to include
		each filename in turn, from $(path copy0.txt) to $(path copy1.txt), etc.

		Once the command line looks good you can either press $(kbd '<Enter>') to run it,
		or continue typing more arguments.  At each point you can always hit
		$(kbd '<TAB>') and Zsh will provide more completions.

		If you type something that matches no files, Zsh will just ignore
		$(kbd '<TAB>')s, no matter how hard and furiously you press it.

		:

		_tutr_pressanykey

		cat <<-:

		At least, I'm pretty sure that is what will happen for you.  Because Zsh
		is so customizable, it's possible that you'll experience different
		behavior.  There's only one way to find out!

		:
	else
		cat <<-:

		You are using Bash, and tab completion works like this:

		All 3 of the '$(bld c)' files' names have the first 4 letters in common.  When
		you first press $(kbd '<TAB>'), Bash fills in as much as is common among those
		files, so '$(bld c)' becomes '$(bld copy)'.  The names of the three files which start
		with '$(bld copy)' are displayed, and a new command line is printed below this.

		:

		_tutr_pressanykey

		cat <<-:

		If you continue to press $(kbd '<TAB>'), the same choices, along with a new
		command line, are re-printed going down the screen.

		Once the command line looks good you can either press $(kbd '<Enter>') to run it,
		or continue typing more arguments.  At each point you can always hit
		$(kbd '<TAB>') and Bash will provide more completion options.

		:
	fi

	_tutr_pressanykey

	cat <<-:

	Try it now!  Use tab completion to help you write the command
	  $(cmd ls copy0.txt copy1.txt copy2.txt)

	Be lazy and let ${_SH} do most of the typing!
	:
}

tabcmplt_ls_c_test() {
	_tutr_generic_test -c ls -a copy0.txt -a copy1.txt -a copy2.txt -d "$_BASE"
}

tabcmplt_ls_c_hint() {
	_tutr_generic_hint $1 ls "$_BASE"
	cat <<-:

	Use ${_SH}'s tab completion to help you type out the command
	  $(cmd ls copy0.txt copy1.txt copy2.txt)
	:
}

tabcmplt_ls_c_epilogue() {
	cat <<-:
	Are you getting the hang of it yet?

	:
	_tutr_pressanykey
}



tabcmplt_cp_c_rw() {
	command rm -f "$_BASE"/new.txt
}

tabcmplt_cp_c_ff() {
	create_copy0_txt
	command cp -f "$_BASE"/copy0.txt "$_BASE"/new.txt
}

tabcmplt_cp_c_prologue() {
	cat <<-:
	What if you ask ${_SH} to complete a filename that doesn't exist?

	This time I want you to run
	  $(cmd cp copy0.txt new.txt)

	First, enter $(cmd cp c)$(kbd '<TAB>').  Tab completion saves you typing by completing
	this to $(cmd cp copy), and a few more keystrokes can get you to
	  $(cmd cp copy0.txt)

	:

	_tutr_pressanykey

	if [[ -n $ZSH_NAME ]]; then
		cat <<-:
		From $(cmd cp copy), you need to type another character to narrow down the
		remaining possibilities:

		* If you press $(kbd '<TAB>'), then Zsh cycles through the filenames it sees.
		* If you type '$(bld 0)' followed by $(kbd '<TAB>'), then Zsh can finish $(path copy0.txt).
		* If you type a '$(bld 1)' at that juncture, Zsh will complete that to
		  $(path copy1.txt).
		* If you type something that doesn't match any files, then Zsh does
		  nothing.

		:

	else
		cat <<-:

		From $(cmd cp copy), you need to type another character to narrow down the
		remaining possibilities:

		* If you type '$(bld 0)' followed by $(kbd '<TAB>'), then Bash can finish $(path copy0.txt).
		* If you type a '$(bld 1)' at that juncture, Bash will complete that to
		  $(path copy1.txt).
		* If you type something that doesn't match any files, Bash rings the
		  terminal's bell to get your attention.

		Sometimes the bell is an audible beep, or your screen may flash.
		Or nothing will happen.  It depends on your Terminal.

		:

	fi

	_tutr_pressanykey

	cat <<-:

	But ${_SH} can't read your mind; it can only look at the files that are
	here.  It is not able to take you from
	  $(cmd cp copy0.txt n)$(kbd '<TAB>')
	to
	  $(cmd cp copy0.txt new.txt)

	You'll just have to write that part yourself.

	:

	_tutr_pressanykey
}

tabcmplt_cp_c_test() {
	_tutr_generic_test -c cp -a copy0.txt -a new.txt -d "$_BASE"
}

tabcmplt_cp_c_hint() {
	_tutr_generic_hint $1 cp "$_BASE"

	cat <<-:

	Run $(cmd cp copy0.txt new.txt) to proceed.
	:
}

tabcmplt_cp_c_epilogue() {
	cat <<-:
	Nicely done!

	:
	_tutr_pressanykey
}



tabcmplt_cmd_prologue() {
	cat <<-:
	I have one more tab completion trick to share with you.  Did you know
	that ${_SH} can complete the names of commands?

	Now, I know what you're thinking - "just how lazy are these programmers
	who can't be bothered to type out $(cmd ls), $(cmd cp) or $(cmd rm) by themselves?
	And, moreover, isn't $(cmd l)$(kbd '<TAB>') just as much work as $(cmd ls)?"

	You aren't wrong.  But consider that
	  "$(cmd ${_LONGEST})"
	is the longest command on your system.  If you needed to run this
	program, would you want to type its name out, in full, every time?
	Would you even remember how to spell it?

	You can probably remember that it starts with "$(cmd ${_LONGEST:0:7})", which
	is enough for ${_SH} to help you narrow it down.

	:

	_tutr_pressanykey

	cat <<-:

	Now, the completion output of $(cmd l)$(kbd '<TAB>') is going to be overwhelming:
	Your computer probably has hundreds of programs with names starting with
	that letter.  When ${_SH} detects that there are more than 100 or so
	possibilities, it will give you a prompt that looks like this:
	:
	if [[ -n $ZSH_NAME ]]; then
		cat <<-:
		  $(bld "zsh: do you wish to see all ### possibilities (## lines)?")

		Again, because of customizations, your mileage may vary.  You could
		very well see something different, or no prompt at all.
		:
	else
		echo "  $(bld "Display all ### possibilities? (y or n)")"
	fi

	echo
	_tutr_pressanykey

	cat <<-:

	For this challenge, I want you to find the command whose name starts
	with '$(bld w)' and that prints out your username.  Use tab completion to
	narrow down the field until you find the right one.
	:

}


tabcmplt_cmd_test() {
	_WRONG_LETTER=99
	_COLD=98
	_GETTING_WARMER=97
	_GOOGLE=96
	_UM_NO=95
	_SO_CLOSE=94

	[[ ${_CMD[@]} == whoami ]] && return 0
	[[ ${_CMD[@]} == "who am i" ]] && return $_SO_CLOSE
	[[ ${_CMD[@]} == whois ]] && return $_UM_NO
	[[ ${_CMD[0]} == who* ]] && return $_GETTING_WARMER
	[[ ${_CMD[@]} == "id -un" ]] && return $_GOOGLE
	[[ ${_CMD[@]} == 'echo $USER' ]] && return $_GOOGLE
	[[ ${_CMD[0]} == id ]] && return $_GOOGLE
	[[ ${_CMD[0]} = w* ]] && return $_COLD
	[[ ${_CMD[0]} != w* ]] && return $_WRONG_LETTER
	return $_UM_NO
}

tabcmplt_cmd_hint() {
	case $1 in
		$PASS) ;;

		$_COLD)
			cat <<-:
			I'll give you a hint: the name of the command I'm thinking of 
			has six letters.
			:
			;;

		$_SO_CLOSE)
			cat <<-:
			You are soooo close.
			:
			;;

		$_WRONG_LETTER)
			cat <<-:
			You're not even in the right ballpark!

			The command you are looking for starts with a '$(bld w)'
			:
			;;

		$_GETTING_WARMER)
			cat <<-:
			You're on the right track!  Keep going!
			:
			;;

		$_GOOGLE)
			cat <<-:
			Are you trying to solve this with Google, or something?
			:
			;;

		*)
			cat <<-:
			  , ; ,   .-'"""'-.   , ; ,
			  \\\\|/  .'         '.  \\|//
			   \\-;-/   ()   ()   \\-;-/    LOL WUT?
			   // ;               ; \\\\
			  //__; :.         .; ;__\\\\
			 \`-----\\'.'-.....-'.'/-----'
			        '.'.-.-,_.'.'
			jgs       '(  (..-'
			            '-'
			:
			;;

	esac
}

tabcmplt_cmd_epilogue() {
	_tutr_pressanykey

	cat <<-:

	If nothing else, I hope that you are beginning to realize that ${_SH}
	isn't so hard to use after all.  And ${_SH} has many more time-saving
	shortcuts hidden beneath the surface!  There are so many that even *I*
	don't know them all!

	Given enough time and practice, tab completion will become an automatic
	reflex.  You will find yourself hitting $(kbd '<TAB>') in other programs.

	Keep practicing tab completion through the rest of these lessons, and
	you'll be a command-line wizard in no time!

	Now, let's get back to manipulating files.
	:

	_tutr_pressanykey
}



move01_rw() {
	create_move0_txt
	command rm -f "$_BASE/move1.txt"
}

move01_ff() {
	command mv -f "$_BASE/move0.txt" "$_BASE/move1.txt"
}

move01_prologue() {
	cat <<-:
	The Unix command to move files is named $(cmd mv).
	Its syntax is just like $(cmd cp):

	  $(cmd mv SOURCE DESTINATION)

	Example: move a file $(path hello.txt) to $(path world.doc)
	  $(cmd mv hello.txt world.doc)

	After running $(cmd mv) the SOURCE file no longer exists.  Be careful!!

	Use $(cmd mv) to move the file $(path move0.txt) to $(path move1.txt).
	You'll notice that pressing Tab will complete the filename $(path move0.txt),
	but not $(path move1.txt).
	:
}

move01_test() {
	_MOVE0_LOST=99
	_DUP_ARGS=98
	[[ ! -f $_BASE/move0.txt && -f $_BASE/move1.txt ]] && return 0
	[[ ! -f $_BASE/move0.txt ]] && return $_MOVE0_LOST
	[[ ${_CMD[0]} == mv && ${_CMD[1]} == move0.txt && ${_CMD[2]} == move0.txt ]] && return $_DUP_ARGS
	_tutr_generic_test -c mv -a move0.txt -a move1.txt -d "$_BASE"
}

move01_hint() {
	case $1 in
		$_MOVE0_LOST)
			create_move0_txt

			cat <<-:
			What happened to $(path move0.txt)?
			No matter, I've replaced it for you.  Please try again.
			:
			;;

		$_DUP_ARGS)
			cat <<-:
			Look closely at the command you typed.  Does that even make sense?

			Did tab completion lead you astray?  You'll just have to type the second
			filename all by yourself.
			:
			;;
		*)
			_tutr_generic_hint $1 mv "$_BASE"
			;;
	esac

	cat <<-:

	Use $(cmd mv) to move the file $(path move0.txt) to $(path move1.txt).
	Your command should look like this:

	  $(cmd mv move0.txt move1.txt)
	:
}


move02_err_prologue() {
	cat <<-:
	Now use the move command to move $(path move0.txt) to $(path move2.txt).
	:
}

move02_err_test() {
	_MOVED_MOVE1=99
	_DUP_ARGS=98

	if [[ -f "$_BASE/move2.txt" && ! -f "$_BASE/move1.txt" ]]; then
		mv "$_BASE/move2.txt" "$_BASE/move1.txt"
		return $_MOVED_MOVE1
	elif [[ ${_CMD[0]} == mv && ${_CMD[1]} == move1.txt && ${_CMD[2]} == move1.txt ]]; then return $_DUP_ARGS
	else _tutr_generic_test -f -c mv -a move0.txt -a move2.txt -d "$_BASE"
	fi
}

move02_err_hint() {
	case $1 in
		$_MOVED_MOVE1)
			cat <<-MSG
			You ran the wrong command.  You moved $(path move1.txt) to $(path move2.txt), which
			is not the command I asked you to run.

			Did tab completion lead you astray?  On this step you'll have to type
			these filenames all by yourself.

			I'm going to put things back by moving $(path move2.txt) to $(path move1.txt) so you
			can try again.
			MSG
			;;
		$_DUP_ARGS)
			cat <<-:
			Look closely at the command you typed.  Does that even make sense?

			Did tab completion lead you astray?  You'll just have to type both
			arguments all by yourself.
			:
			;;
		*)
			_tutr_generic_hint $1 mv "$_BASE"
			;;
	esac

	cat <<-:

	Use $(cmd mv) to move the file $(path move0.txt) to $(path move2.txt).
	Your command should look like this:
	  $(cmd mv move0.txt move2.txt)
	:
}

move02_err_epilogue() {
	_tutr_pressanykey

	cat <<-:

	What happened here?

	You previously moved $(path move0.txt) to the name $(path move1.txt), so $(path move0.txt)
	was not available to be moved to $(path move2.txt).

	Another way to think of this is to consider moving a file to be
	equivalent to $(bld COPYING) a file and then $(bld REMOVING) the original.

	Essentially, moving a file gives it a new name.  For this reason Unix
	does not have a dedicated command to rename files.  Renaming a file is
	the same thing as moving it to a new name.

	:
	_tutr_pressanykey
}


move12_rw() {
	command rm -f "$_BASE/move1.txt" "$_BASE/move2.txt"
	create_move0_txt
	command mv -f "$_BASE/move0.txt" "$_BASE/move1.txt"
}

move12_ff() {
	create_move0_txt
	command cp -f "$_BASE/move0.txt" "$_BASE/move1.txt"
	command mv "$_BASE/move0.txt" "$_BASE/move2.txt"
}

move12_prologue() {
	cat <<-:
	Let's try that again, but with a file that exists.

	Use the $(cmd mv) command to rename $(path move1.txt) to $(path move2.txt).
	:
}

move12_test() {
	[[ ! -f $_BASE/move1.txt && -f $_BASE/move2.txt ]] && return 0
	[[ ! -f $_BASE/move1.txt ]] && return 99
	_tutr_nonce && return $PASS
	_tutr_generic_test -c mv -a move1.txt -a move2.txt -d "$_BASE"
}

move12_hint() {
	case $1 in
		99)
			create_move0_txt
			mv $_BASE/move0.txt $_BASE/move1.txt 

			cat <<-:
			What happened to $(path move1.txt)?
			It doesn't matter, I've replaced it for you.  Try again.
			:
			;;

		*)
			_tutr_generic_hint $1 mv "$_BASE"
			;;
	esac

	cat <<-:

	Try running
	  $(cmd mv move1.txt move2.txt)
	:
}



mv_clobber_pre() {
	[[ ! -f $_BASE/different.txt ]] && create_different_txt
	if [[ ! -f $_BASE/move2.txt ]]; then
		create_move0_txt
		mv $_BASE/move0.txt $_BASE/move2.txt
	fi
}

mv_clobber_prologue() {
	cat <<-:
	By now you will have noticed that these commands don't output messages
	when they succeed.  As we say in Unix, no news is good news!

	But when a command does have something to say, you should pay attention!

	The move command also carries a risk of overwriting the destination
	file.  As with $(cmd cp), $(cmd mv) MAY stop and ask for confirmation before doing
	anything destructive.

	Find out what happens in ${_SH} when you move the file $(path move2.txt)
	onto $(path different.txt).
	:
}

mv_clobber_test() {
	_tutr_nonce && return $PASS
	[[ ! -f $_BASE/move2.txt && ! -f $_BASE/different.txt ]] && return 99
	_tutr_generic_test -c mv -a move2.txt -a different.txt -d "$_BASE"
}

mv_clobber_hint() {
	case $1 in
		99)
			mv_clobber_pre
			cat <<-:
			Huh, what happened to those files?
			Oh well, I've replaced them.  Do try again.
			:
			;;
		*)
			_tutr_generic_hint $1 mv "$_BASE"
			mv_clobber_pre
			;;
	esac

	cat <<-:

	Find out what happens on your computer when you overwrite the file
	$(path different.txt) with $(path move2.txt):

	  $(cmd mv move2.txt different.txt)
	:
}

mv_clobber_epilogue() {
	cat <<-:
	It should scare you a little bit that these commands can so easily
	clobber other files.

	I hope that you get into the habit of closely considering your commands
	before hitting $(kbd ENTER) and reading all prompts before answering.

	:
	_tutr_pressanykey
}


rm_1file_rw() {
	create_copy0_txt
}

rm_1file_ff() {
	command rm -f "$_BASE/copy0.txt"
}

rm_1file_pre() {
	create_copy0_txt
}

rm_1file_prologue() {
	cat <<-:
	The Unix command to delete or remove files is called $(cmd rm).
	This is its syntax:

	  $(cmd rm '[-f] FILENAME...')

	Example: remove the files $(path hello.txt) and $(path world.doc)
	  $(cmd rm '"hello.txt"' '"world.doc"')

	$(cmd rm) expects at least one argument that is the name of a file, but
	several filenames may be given.  $(cmd rm) may also take the $(cmd -f) option,
	which will be explained shortly.

	Files removed by $(cmd rm) are $(bld PERMANENTLY) gone.  There is no recycle bin or
	"undelete" feature on Unix; these commands are serious business.  Like
	$(cmd cp) and $(cmd mv), $(cmd rm) may ask for confirmation before making permanent
	changes.  If it does, you will respond in the same way as the other
	commands you just learned.

	We'll start small by removing a single file.
	Remove the file $(path copy0.txt).
	:
}

rm_1file_test() {
	if   _tutr_nonce; then return $PASS
	elif [[ ! -f $_BASE/copy0.txt ]]; then return 0
	elif [[ ${_CMD[@]} = "rm copy0.txt" && -f $_BASE/copy0.txt ]]; then return 99
	else _tutr_generic_test -c rm -a copy0.txt -d "$_BASE"
	fi
}

rm_1file_hint() {
	case $1 in
		99)
			cat <<-:
			Hmm, your command looks okay, but $(path copy0.txt) is still here.
			Did $(cmd rm) prompt you but you didn't respond affirmatively?

			Why don't you try this again.
			:
			;;
		*)
			_tutr_generic_hint $1 rm "$_BASE"
			;;
	esac

	cat <<-:

	Remove the file $(path copy0.txt).
	  $(cmd rm copy0.txt)
	:
}





rm_star0_rw() {
	create_copy0_txt
	command cp -f "$_BASE/copy0.txt" "$_BASE/copy1.txt"
	command cp -f "$_BASE/copy0.txt" "$_BASE/new.txt"
	command mv -f "$_BASE/copy0.txt" "$_BASE/copy2.txt"
	create_move0_txt
	command mv -f "$_BASE/move0.txt" "$_BASE/move2.txt" 
	create_different_txt
}

rm_star0_ff() {
	command rm -f "$_BASE/"*.txt
}

rm_star0_prologue() {
	cat <<-:
	Suppose you need to delete 100 $(bld .txt) files from a directory.

	To delete multiple files in a graphical file browser you would highlight
	them with the mouse and either hit the $(kbd DELETE) key or drag them into some
	representation of a garbage can.

	To remove files in the shell you run $(cmd rm) with each target file's name
	as arguments.  Even with $(kbd TAB) completion this would be tedious to do for
	100 different files.  What you need is a way to tell the shell to run
	$(cmd rm) on every file with a name ending in $(bld .txt).

	The shell feature that lets you do this is called "$(bld wildcards)".  When
	given a wildcard the shell substitutes it with all matching filenames
	and THEN runs your command.  The shell has a few wildcards up its sleeve,
	but I will teach you the one that serves 90% of your needs.

	The asterisk '$(bld '*')' (a.k.a. "glob", a.k.a. "star") matches ANY number of
	characters in a file's name:

	  $(cmd rm '*.txt')   => run $(cmd rm) on all files with the $(bld .txt) extension
	  $(cmd rm 'l*.txt')  => run $(cmd rm) on all files beginning with the letter '$(bld l)' and
	                ending in $(bld .txt)
	  $(cmd rm '*t*')     => run $(cmd rm) on all files with a $(bld t) anywhere in their names
	  $(cmd rm '*')       => run $(cmd rm) on all files here; very dangerous!

	Use a glob pattern with $(cmd rm) to remove all files with the $(bld .txt)
	extension from this directory.
	:
}

rm_star0_test() {
	if [[ -n $ZSH_NAME ]]; then
		setopt local_options nullglob
	fi
	if   _tutr_nonce; then return $PASS
	elif [[ -n $ZSH_NAME  && -z $(echo *.txt) ]]; then return 0
	elif [[ -n $BASH_VERSION && $(echo *.txt) = "*.txt" ]]; then return 0
	elif [[ ${_CMD[0]} = rm ]]; then return 99
	else _tutr_generic_test -c rm -a "*.txt" -d "$_BASE"
	fi
}

rm_star0_hint() {
	case $1 in
		99)
			cat <<-:
			There still exist one or more $(bld .txt) files after that command.
			You'll need to try again.
			:
			;;
		*)
			_tutr_generic_hint $1 rm "$_BASE"
			;;
	esac

	cat <<-:

	Use a glob pattern to run $(cmd rm) on all files whose names end in $(bld .txt).
	
	Run $(cmd tutor hint) to see the shell wildcard explanation again.
	:
}

rm_star0_epilogue() {
	cat <<-:
	Wildcards can let you be more efficient at removing many related files
	than with a GUI.
	
	But being efficient when it comes to deleting files cuts both ways;
	in just a few keystrokes you can obliterate $(bld ALL) of your precious work!  

	:
	_tutr_pressanykey
}



rm_star1_pre() {
	# re-create any missing JPG or WAV files
	for I in {a..z}; do
		touch "$_BASE/$I.jpg"
	done

	for I in {0..9}; do
		touch "$_BASE/$I.wav"
	done
}

rm_star1_rw() {
	command rm -f "$_BASE/"*.jpg  "$_BASE/"*.wav
}
rm_star1_ff() {
	rm_star1_pre
	command rm -f "$_BASE/"*.wav
}

rm_star1_prologue() {
	cat <<-:
	I just created 26 $(bld .jpg) and 10 $(bld .wav) files in this directory.

	You can use $(cmd ls) to see their names.  

	Use $(cmd rm) with a wildcard to remove all of the $(bld .wav) files with a single
	command.  Leave the $(bld .jpg) files alone.
	:
}

rm_star1_test() {
	_WAVS_LEFT_BEHIND=99
	_WRONG_FILES=98
	_STAR=97
	_TOO_MUCH=96

	if   _tutr_nonce; then return $PASS
	elif [[ "$PWD" != "$_BASE" ]]; then return $WRONG_PWD
	elif [[ ${_CMD[0]} == rm && ${_CMD[1]} = "*" ]]; then return $_STAR
	elif [[ -n $ZSH_NAME ]]; then
		setopt local_options nullglob
		local JPG=$(echo *.jpg)
		local WAV=$(echo *.wav)
		if   [[ -n $JPG && -z $WAV ]]; then return 0
		elif [[ -z $JPG && -n $WAV ]]; then return $_WRONG_FILES
		elif [[ -z $JPG && -z $WAV ]]; then return $_TOO_MUCH
		elif [[ ${_CMD[0]} = rm && -n $WAV ]]; then return $_WAVS_LEFT_BEHIND
		fi
	else
		local JPG=$(echo *.jpg)
		local WAV=$(echo *.wav)
		if   [[ $JPG != "*.jpg" && $WAV  = "*.wav" ]]; then return 0
		elif [[ $JPG  = "*.jpg" && $WAV != "*.wav" ]]; then return $_WRONG_FILES
		elif [[ $JPG  = "*.jpg" && $WAV  = "*.wav" ]]; then return $_TOO_MUCH
		elif [[ ${_CMD[0]} = rm && $WAV != "*.wav" ]]; then return $_WAVS_LEFT_BEHIND
		fi
	fi
	_tutr_generic_test -c rm -a *.wav -d "$_BASE"
}

rm_star1_hint() {
	case $1 in
		$_WAVS_LEFT_BEHIND)
			cat <<-:
			That command left some $(bld .wav) files behind.
			You'll need to try again.
			:
			;;

		$_WRONG_FILES)
			rm_star1_pre
			cat <<-:
			I think that you deleted the wrong files!
			I've put things back so you can try again.
			:
			;;

		$_TOO_MUCH)
			rm_star1_pre
			cat <<-:
			Whoa, easy there!  That was much too much.
			I've put things back so you can try again.
			:
			;;

		$_STAR)
			rm_star1_pre
			rm_star0_rw
			cat <<-:
			You deleted everything!
			I've put things back so you can try again.
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_BASE"
			return
			;;

		*)
			_tutr_generic_hint $1 rm "$_BASE"
			;;
	esac

	cat <<-:

	Run $(cmd rm) with a wildcard to remove all $(bld .wav) files in a single command.
	Leave the $(bld .jpg) files alone.

	Be ready to press '$(kbd y)' a whole bunch of times!
	:
}


rm_star1_epilogue() {
	cat <<-:
	They gone.

	:

	_tutr_pressanykey
}



rm_star2_pre() {
	rm -rf "$_BASE"/{a..z}{a..z}.png "$_BASE"/{0..9}{0..9}.mp4
	touch "$_BASE"/{a..z}{a..z}.png "$_BASE"/{0..9}{0..9}.mp4
}


rm_star2_rw() {
	rm -rf "$_BASE"/{a..z}{a..z}.png "$_BASE"/{0..9}{0..9}.mp4
}

rm_star2_ff() {
	rm -rf "$_BASE"/{a..z}{a..z}.png "$_BASE"/{0..9}{0..9}.mp4
}

rm_star2_prologue() {
	cat <<-:
	To simulate your Downloads folder (a.k.a. the junk-drawer for computers)
	I have just created 100 $(bld .mp4) and 676 $(bld .png) files.  You can count them
	if you don't believe me.  I'm going to be a big jerk and make YOU clean
	up MY mess.

	Does $(bld rm) prompt for confirmation on your computer?  If so, you're
	looking at pressing '$(kbd y)' 776 times.  That is exactly the sort of tedious,
	repetitive thing that a computer ought to do for you.

	This is where $(cmd rm)'s "$(bld force)" option $(cmd -f) comes in handy.
	$(cmd rm -f) temporarily disables confirmation prompts in favor of a
	"silent-but-deadly" mode of operation.

	Example: to forcibly remove all Markdown files in one command:
	  $(cmd rm -f '*.md')

	:
	_tutr_pressanykey

	cat <<-:

	$(bld BE VERY CAREFUL WITH) $(cmd rm -f)$(bld '!!!')

	Every Unix hacker has their own harrowing story about this command.
	Don't become another statistic!  Think before you type, and think again
	before you hit $(kbd ENTER).  $(bld Check yourself before you wreck yourself.)

	If you do find yourself answering hundreds of prompts, cancel the
	command with '$(kbd '^C')' and try again.  Understand that all files for which
	you answered "$(bld yes)" are already deleted.

	Are you ready to get dangerous?
	Remove all $(bld .png) and $(bld .mp4) files, leaving all other files unscathed.
	:
}

rm_star2_test() {
	if [[ -n $ZSH_NAME ]]; then
		setopt local_options nullglob
	fi
	if   _tutr_nonce; then return $PASS
	elif [[ -n $ZSH_NAME && -z $(echo *.jpg) ]]; then return 98
	elif [[ -n $BASH_VERSION && $(echo *.jpg) == "*.jpg" ]]; then return 98
	elif [[ -n $ZSH_NAME && -z $(echo *.png) && -z $(echo *.mp4) ]]; then return 0
	elif [[ -n $BASH_VERSION && $(echo *.png *.mp4) = "*.png *.mp4" ]]; then return 0
	elif [[ ${_CMD[0]} = rm ]]; then return 99
	else _tutr_generic_test -c rm -a *.png -a *.mp4 -d "$_BASE"
	fi
}

rm_star2_hint() {
	case $1 in
		99)
			cat <<-:
			There are still some $(bld .png) or $(bld .mp4) files here.
			You'll need to try again.
			:
			;;
		98)
			cat <<-:
			Woah there buckaroo! You went a little crazy with that command and
			deleted the $(bld .jpg) files as well. I'm going to get things back to how
			they should be, then you can try again.
			:
			rm_star2_pre
			rm_star1_ff
			;;
		*)
			_tutr_generic_hint $1 rm "$_BASE"
			;;
	esac

	cat <<-:

	Use $(cmd rm) with a wildcard to remove all 776 of the $(bld .png) and $(bld .mp4)
	files that I have created.

	  $(cmd rm -f '*.png') $(cmd '*.mp4')

	The $(cmd -f) option will let you avoid pressing "$(kbd y)" 776 times.
	:
}

rm_star2_epilogue() {
	cat <<-:
	How is THAT for brute efficiency?

	Imagine doing that same task in a graphical file manager.  How much
	scrolling and dragging would it take to select that many files?

	And what if you accidentally highlighted a few $(bld .pdf) or $(bld .docx) files?

	I hope this gives you a glimpse of the power of the command shell.

	:
	_tutr_pressanykey
}



rm_star3_ff() {
	command rm -f "$_BASE"/*
}

rm_star3_prologue() {
	cat <<-:
	For your last trick, use a bare glob '$(bld '*')' to delete $(bld EVERYTHING) else in
	here.

	The '$(bld '*')' pattern matches $(bld EVERY) file except for hidden ones.  Recall from
	Lesson #0 that hidden files have names that begin with a dot ('.').

	Add the $(cmd -f) option so you don't need to press "$(kbd y)" so many times.
	:
}

rm_star3_test() {
	if [[ -n $ZSH_NAME ]]; then
		setopt local_options nullglob
	fi

	if [[ "$PWD" != "$_BASE" ]]; then return $WRONG_PWD
	elif _tutr_nonce; then return $PASS
	elif [[ -n $ZSH_NAME && -z $(echo *) ]]; then return 0
	elif [[ -n $BASH_VERSION && $(echo *) = "*" ]]; then return 0
	elif [[ ${_CMD[0]} = rm ]]; then return 99
	else _tutr_generic_test -c rm -a -f -a * -d "$_BASE"
	fi
}

rm_star3_hint() {
	case $1 in
		99)
			cat <<-:
			There are still files in here.
			These files are all junk anyway.  What are you waiting for?
			:
			;;

		$WRONG_PWD)
			_tutr_minimal_chdir_hint "$_BASE"
			return
			;;
		*)
			_tutr_generic_hint $1 rm "$_BASE"
			;;
	esac

	cat <<-:

	Use a bare glob '$(bld '*')' to delete all non-hidden files in this directory.
	You can combine this with the $(cmd -f) option to avoid pressing "$(kbd y)" so many
	times:
	  $(cmd 'rm -f *')
	:
}


rm_star3_epilogue() {
	cat <<-:
	Now that you've run that command, be careful the next time you feel it 
	go out of your fingertips.  That is one command that should give you a
	sinking feeling each time you see it.

	Remember, in Unix file deletion is forever!

	:
	_tutr_pressanykey
}


case $_PLAT in
	WSL)
		source main.sh && _tutr_begin \
			copy01 \
			copy02 \
			list1 \
			cp_clobber \
			tabcmplt_ls_d \
			tabcmplt_ls_c \
			tabcmplt_cp_c \
			move01 \
			move02_err \
			move12 \
			mv_clobber \
			rm_1file \
			rm_star0 \
			rm_star1 \
			rm_star2 \
			rm_star3
		;;

	*)
		source main.sh && _tutr_begin \
			copy01 \
			copy02 \
			list1 \
			cp_clobber \
			tabcmplt_ls_d \
			tabcmplt_ls_c \
			tabcmplt_cp_c \
			tabcmplt_cmd \
			move01 \
			move02_err \
			move12 \
			mv_clobber \
			rm_1file \
			rm_star0 \
			rm_star1 \
			rm_star2 \
			rm_star3
		;;
esac

# vim: set filetype=sh noexpandtab tabstop=4 shiftwidth=4 textwidth=76 colorcolumn=76:
