#!/bin/sh

_HELP="Lesson #3.1 Topics
==================
* Configure your WSL environment with QOL changes
* Create an alias for the Windows Python executable
* Setup shortcuts to easily access common directories

Commands used in this lesson
============================
* cd
* ls -l
"

PATH="$PWD/.lib:$PATH"
. shell-compat-test.sh

source record.sh
source ansi-terminal-ctl.sh
if [[ -n $_TUTR ]]; then
	source generic-error.sh
	source nonce.sh
fi

setup() {
	source platform.sh
	[[ $_PLAT == WSL ]] || _tutr_die _not_wsl_message

	if _tutr_record_exists ${_TUTR#./}; then
		_tutr_warn printf "'You have already completed this lesson'"
		if ! _tutr_yesno "Would you like to do it again?"; then
			_tutr_info printf "'SEE YOU SPACE COWBOY...'"
			exit 1
		fi
	fi
	source screen-size.sh 80 30

	# No base is needed for this lesson, as we just go to the home directory
	export _TUTR_DIR=$PWD


	[[ -L /usr/bin/python && $(realpath /usr/bin/python) == *python.exe ]]
	export _HAS_PY_EXE_LINK=$?

	if   which python3 &>/dev/null && [[ $(python3 -V 2>&1) = "Python 3"* ]]; then
		export _PY=python3
	elif which python &>/dev/null && [[ $(python -V 2>&1) = "Python 3"* ]]; then
		export _PY=python
	else
		export _PY=NONE
	fi

}

_not_wsl_message() {
	cat <<-MSG
	It appears that you're $(bld not) using Windows Subsystem For Linux (WSL)!
	
	This lesson isn't for you. It is designed to help those using WSL to
	configure their environment. This lesson is $(bld '*not*') required to be
	completed for the certificate of completion, so don't fret.

	Exiting this lesson now...
	MSG
}

_has_pythonexe_link_message() {
	cat <<-MSG
	It appears you've already got a link that points to $(cmd python.exe)!
	We're going to continue on past this step that would have asked you to
	give me permission to make this link for you. You don't need to add it,
	you've already done it!
	MSG
	_tutr_pressanykey
}

_no_python_skip_link() {
	cat <<-MSG
	We're going to skip the step that has you add a link to $(cmd python.exe) as
	you don't have the Windows version of python properly installed.
	MSG
	_tutr_pressanykey
}

getWinVars() {
    local WINVAR
    case $1 in
        username)
            WINVAR=%USERNAME%
            ;;
        drive|systemdrive)
            WINVAR=%SystemDrive%
            ;;
        userprofile|*)
            WINVAR=%USERPROFILE%
            ;;
    esac

    cmd.exe /c "echo $WINVAR" 2>/dev/null | sed "s/\r//g"
}



prologue() {
	[[ -z $DEBUG ]] && clear
	echo
	cat <<-PROLOGUE
	Shell Lesson #3.1: Set up your WSL environment

	In this lesson you will

	* Configure your WSL environment
	* Make some Quality-of-Life improvements
	* Create a shortcut to the Windows Python installation
	* Create shortcuts to easily access common directories

	Let's get started!

	PROLOGUE

	_tutr_pressanykey
	clear

	cat <<-PROLOGUE

	When working with WSL, there is actually a "barrier" between your
	standard Windows OS and the WSL environment. WSL is like a virtual
	machine; a separate "virtual computer" running inside your operating
	system. WSL isn't *actually* a virtual machine, but we're not going to
	get into the nuances of how WSL works here. Feel free to look that up
	on your own.

	Because of this "barrier" between the two systems, the files stored in
	Windows and the files stored in WSL are not easily accessible out of
	the box. We are going to fix that for you in this lesson. This barrier
	also prevents us from using WSL commands in Windows, and adds an extra
	step to use Windows commands/programs from within WSL. We are going to
	teach you how to use Windows programs from within WSL, and give you an
	easy way to access the Windows version of Python you downloaded from
	Python.org from within WSL.

	Let's get started!
	PROLOGUE
	_tutr_pressanykey
}


cleanup() {
	# Remember that this lesson has been completed
	(( $# >= 1 && $1 == $_COMPLETE)) && _tutr_record_completion ${_TUTR#./}
	echo "You worked on the WSL Setup Lesson for $(_tutr_pretty_time)"
}

epilogue() {
	cat <<-'EPILOGUE'
	  _____                        __       __     __  _             
	 / ___/__  ___  ___ ________ _/ /___ __/ /__ _/ /_(_)__  ___  ___
	/ /__/ _ \/ _ \/ _ `/ __/ _ `/ __/ // / / _ `/ __/ / _ \/ _ \(_-<
	\___/\___/_//_/\_, /_/  \_,_/\__/\_,_/_/\_,_/\__/_/\___/_//_/___/
	              /___/                                              
	EPILOGUE
	
	cat <<-EPILOGUE
	Great work! Your WSL environment is all set up for you to have easy
	access to your Windows version of Python, as well as easy access to the
	files on your Windows file system.


	Run $(cmd ./4-projects.sh) to enter your next lesson
	EPILOGUE

	_tutr_pressanykey
}

wsl_installed_python_prologue() {
	[[ $_PY == NONE ]] && return 0

	cat <<-MSG
	We are going to set up Python properly in WSL shortly. But first, I
	want to introduce you to the version of Python that you have
	installed by default in WSL. This version of Python is NOT the same
	as the version of Python you might have installed from Python.org.

	To use your WSL distributions version of Python, we can run the command
	$(cmd $_PY). This will put you into a new Python shell called the $(bld REPL). We
	will learn more about it later. To exit that shell, $(cyn run the command) 
	$(cmd 'exit()') $(cyn inside of that shell.) This will exit the Python REPL.
	
	Run this $(cmd $_PY) command now to see your WSL version of Python, then
	$(cmd 'exit()') out of the Python shell that pops up.
	MSG
}

wsl_installed_python_test() {
	[[ $_PY == NONE ]] && return 0

	_tutr_nonce && return $PASS

	_tutr_generic_test -c $_PY
}

wsl_installed_python_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "$_PY" "$PWD"
			;;
	esac
}

check_python_installed_prologue() {
	cat <<-MSG
	Let's figure out if you have access to the version of Python that you
	installed on Windows. 

	To access Windows programs from within WSL, we *generally* have to
	append the file suffix to the end of the command. In a Windows command
	shell, we could run the Python program by typing $(cmd python), which will
	invoke the program located at a file named $(cmd python.exe). To use this
	Python program from within WSL, we actually have to add the $(bld .exe)
	suffix to our command; invoking it by its full name like an angry mom.

	Let's run the command $(cmd python.exe) now to see if we have properly
	installed Python from $(path Python.org) on Windows. This will start a Python
	shell, and you can then $(cyn run the command) $(cmd 'exit()') $(cyn inside that shell) to
	exit the Python REPL.
	
	$(red IMPORTANT:) If you receive a pop-up that opens you to the Python page
	on the Windows Store, do $(bld NOT) download this program. Exit the Windows
	Store and come back here.
	MSG
}

check_python_installed_test() {
	_tutr_nonce && return $PASS

	_tutr_generic_test -c "python.exe" -i
}

check_python_installed_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "python.exe" "$PWD"
			;;
	esac
}

check_python_installed_epilogue() {
	PY_VERSION=$(python.exe -V 2>/dev/null)

	if [[ -z $PY_VERSION ]]; then
		cat <<-MSG
		It looks like you don't have Python properly installed from Python.org!
		There could be a few things that cause this.

		$(cyn First), if you installed Python from Python.org and haven't restarted
		your system, we will be unable to find the $(cmd python.exe) file in WSL
		until the system gets restarted. If you installed it recently, try
		exiting this lesson, restarting your system, and trying again.
		
		$(cyn Second), during the installation of Python, you may have forgotten to
		select the option "$(bld Add Python to PATH.)" This was *very* important, as
		the $(bld PATH) is how your system shell will find all the commands and
		programs it is to run. You can re-run the Python installation and
		select this option, or you can manually fix it by adding Python to your
		system $(bld PATH). Use a search engine for assistance on how to do that, and
		ensure that you move the path to Python to the $(bld VERY TOP) of the $(bld PATH)
		list. Once that has been done, $(bld restart your computer), and start this
		lesson again.

		$(cyn Third), you could have just *not* installed Python from Python.org.
		That's fine, actually. There exists a version of Python in WSL that you
		can use, as I have introduced you to already. However, you will be
		restricted to *only* using Python in WSL, and might experience some
		strange behaviors later on when you use GUI applications with Python.
		MSG
	else
		cat <<-MSG
		Great! It looks like you have the Windows version of Python installed
		on your system and we can proceed.
		MSG
	fi
	_tutr_pressanykey
}

setup_python_link_prologue() {
	cat <<-MSG
	Now we want to add a special file to your $(path /usr/bin) directory that will
	serve as a link to the $(cmd python.exe) executable, allowing us to run $(cmd python)
	and get $(cmd python.exe).

	To do this, $(bld '*I*') am going to run the following command:
	  $(cmd 'sudo ln -s "$(which python.exe)" /usr/bin/python')

	This command will find $(cmd python.exe) on your system, and then create a
	$(cmd l)i$(cmd n)k to $(cmd python.exe), located at $(path /usr/bin/python). 

	To run this command, I am going to need to use the $(cmd sudo) command. This
	command allows you to tell the $(bld su)per user to $(bld do) something. The
	super user has administrator privileges on the system. Prepending $(cmd sudo)
	to a command runs it as the administrator.

	You need to give me permission to do by running $(cmd sudo -v). You will be
	prompted to enter the password you chose for your Linux user when you
	set up WSL. Enter that password then hit enter. The prompt will not
	change as you type in your password; this is normal.

	$(bld IMPORTANT:) If you would rather not give me temporary super user
	privileges to run the command that makes the link, tell me so by
	running the command $(cmd false). We will continue on with my feelings
	only mildly hurt.
	MSG
}

setup_python_link_test() {
	PY_VERSION=$(python.exe -V 2>/dev/null)

	if [[ $_HAS_PY_EXE_LINK = 0 ]]; then
		_tutr_warn _has_pythonexe_link_message
		return 0
	fi

	if [[ -z $PY_VERSION ]]; then
		_tutr_warn _no_python_skip_link
		return 0
	fi

	_tutr_nonce && return 0

	if [[ ${_CMD[0]} = false ]]; then
		SKIP_MAKING_LINK=true
		return 0
	fi

	_tutr_generic_test -c sudo -a '-v'
}

setup_python_link_hint() {
	case $1 in
		$PASS)
			;;
		$STATUS_FAIL)
			cat <<-MSG
			Running $(cmd sudo -v) failed! Did you incorrectly enter your password?
			Try again!

			If you forgot the password you set up for this account, see:
				$(path 'https://docs.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password')
			MSG
			;;
		*)
			_tutr_generic_hint $1 "sudo -v" "$PWD"
			;;
	esac
	echo
	$_PROLOGUE
	
}

setup_python_link_post() {
	if [[ -n $SKIP_MAKING_LINK ]]; then
		return 0
	fi

	sudo ln -s "$(which python.exe)" /usr/bin/python
	LINK_RETURN_VAL=$?

	[[ -L /usr/bin/python && $(realpath /usr/bin/python) == *python.exe ]]
	export _HAS_PY_EXE_LINK=$?
}

setup_python_link_epilogue() {
	if [[ -n $LINK_RETURN_VAL ]]; then
		case $LINK_RETURN_VAL in
			0)
				cat <<-MSG
				The link was properly made!  It's located at $(path /usr/bin/python).

				If you are curious about it, you can run the command:
				  $(cmd ls -l /usr/bin/python)
				
				This will tell you where the link points to on the right side of the 
				arrow, $(bld "'->'").
				MSG
				_tutr_pressanykey
				;;
			*)
				cat <<-MSG
				Something happened, and I was unable to make the symlink to $(cmd python.exe).

				I'm not exactly sure what happened. If you'd like this link to be created,
				contact erik.falor@usu.edu and we can try to get to the bottom of this.
				
				Continuing on...
				MSG
				_tutr_pressanykey
				;;
		esac
	elif [[ -n $SKIP_MAKING_LINK ]]; then
		cat <<-MSG
		You asked me not to make that link for you, and I obliged. Let's
		continue on.
		MSG
		_tutr_pressanykey
	fi
}

show_windows_filesystem_prologue() {
	cat <<-MSG
	I mentioned earlier that there is a separation between the files you
	have stored in your main Windows OS and WSL. Luckily for us, we can
	easily access your Windows file system from within  WSL.  It's just
	hidden away from us by default.

	We can access your various system drives from within WSL by going to
	the $(path /mnt) directory. This $(path /mnt) directory stands for "mount" and is
	a Linux convention for a place to store all "mounted" filesystems. If
	you were to plug into a USB drive to a Linux computer, you'd
	typically "mount" it and make it accessible within the $(path /mnt) directory.

	Run $(cmd ls /mnt) to see this $(path /mnt) directory.
	MSG
}

show_windows_filesystem_test() {
	if [[ ${_CMD[0]} != ls && _tutr_nonce ]]; then
		return $PASS
	fi

	if [[ "$PWD" == "/mnt" ]]; then
		_tutr_generic_test -c ls
	else
		_tutr_generic_test -c ls -a "/mnt/?$"
	fi
}

show_windows_filesystem_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "ls /mnt" "$PWD"
			;;
	esac
}

show_windows_filesystem_epilogue() {
	DRIVE=$(getWinVars drive)
	cat <<-MSG
	In this $(path /mnt) directory, you should see your '$(bld $DRIVE)' drive; the hard drive
	you have installed Windows on. This is going to be your entry point into
	your Windows file system.
	
	There could also be some directories called $(path wsl) and/or $(path wslg). These
	are created by WSL to help with some of its configuration. No need to
	worry about those if they exist.

	Depending on your computer configuration, you may have a few other
	drives present here. This $(path /mnt) directory is how you will access them.
	MSG
	_tutr_pressanykey
}

move_to_home_prologue() {
	cat <<-MSG
	We're going to give you easy access to some commonly accessed places
	within the Windows file system. To do that though, we need to move to
	your home directory. We are going to place the shortcuts to the
	important places in your Windows file system there so you can have easy
	access to the shortcuts.

	Run $(cmd cd) with no arguments to move home.
	MSG
}

move_to_home_test() {
	_tutr_nonce && return $PASS

	if [[ "$PWD" == "$HOME" ]]; then
		return 0
	else
		_tutr_generic_test -c cd
	fi
}

move_to_home_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "cd" "$PWD"
			;;
	esac
}

make_and_view_symlinks_prologue() {
	cat <<-MSG
	I'm now going to make something called a '$(bld symlink)' for you. This
	symlink is like a "shortcut;" it's a file that points to another file
	location on your computer. I'm going to make a few of these in your
	home directory now. We'll walk through them all momentarily.
	MSG
	_tutr_pressanykey

	if [[ -n $NO_WINHOME ]]; then
		cat <<-MSG
		I was unable to create a link to your Windows Home directory...

		Ummmm WAT.

		This is $(bld very) concerning. How about you reach out to Erik Falor and
		Jaxton Winder for some help?

		Erik Falor:
		  $(path erik.falor@usu.edu)
	
		Jaxton Winder:
		  $(path jaxton.winder@usu.edu)
		MSG
		_tutr_pressanykey
	else
		cat <<-MSG
		Making a symlink to your Windows home directory...

		This link will be called $(path WinHome).
		MSG
		_tutr_pressanykey
	fi

	if [[ -n $NO_WINDOCS ]]; then
		cat <<-MSG
		I was unable to create a link to your Windows Documents directory...

		This isn't the worst thing, but might be an annoyance. Feel free to ask
		a member of DuckieCorp for some help, and maybe they can manually make
		the link for you.
		MSG
		_tutr_pressanykey
	else
		cat <<-MSG
		Making a symlink to your Windows Documents directory...

		This link will be called $(path WinDocs).
		MSG
		_tutr_pressanykey
	fi

	if [[ -n $NO_WINDOWNLOADS ]]; then
		cat <<-MSG
		I was unable to create a link to your Windows Downloads directory...

		This isn't the worst thing, but might be an annoyance. Feel free to ask
		a member of DuckieCorp for some help, and maybe they can manually make
		the link for you.
		MSG
		_tutr_pressanykey
	else
		cat <<-MSG
		Making a symlink to your Windows Downloads directory...

		This link will be called $(path WinDownloads).
		MSG
		_tutr_pressanykey
	fi
	
	if [[ -n $NO_WINDESKTOP ]]; then
		cat <<-MSG
		I was unable to create a link to your Windows Desktop directory...

		There's a *very* good chance it's OneDrive's fault.

		This isn't the worst thing, but might be an annoyance. Feel free to ask
		a member of DuckieCorp for some help, and maybe they can manually make
		the link for you.
		MSG
		_tutr_pressanykey
	else
		cat <<-MSG
		Making a symlink to your Windows Desktop directory...

		This link will be called $(path WinDesktop).
		MSG
		_tutr_pressanykey
	fi

	cat <<-MSG
	
	Now that these links are all created, let's run the command $(cmd ls -l) to
	see these new symlinks and where they point to.
	
	$(bld Note:) The argument to $(cmd ls) is a lowercase L: not a one.
	MSG
}

_warn_cannot_make_link() {
	cat <<-MSG
	$(bld It appears I cannot properly make the link $1 for some reason!)

	You likely don't have the directory I was expecting you to have in the
	location it should be. I'll skip some steps due to this issue. You can
	contact Erik or Jaxton for some help on how to (possibly) fix this. 

	$(bld THIS IS NOT A BUG IN THE SHELL TUTOR.)  It's likely $(red OneDrive)'s fault.

	Erik Falor:
	  $(path erik.falor@usu.edu)
	
	Jaxton Winder:
	  $(path jaxton.winder@usu.edu)
	MSG
}

make_and_view_symlinks_pre() {
	WINDOWS_HOME=$(wslpath "$(getWinVars userprofile)")
	
	if [[ -d "$WINDOWS_HOME" ]]; then
		! [[ -L "$HOME/WinHome" ]] && ln -s "$WINDOWS_HOME" "$HOME/WinHome"
	else
		_tutr_warn _warn_cannot_make_link "$WINDOWS_HOME"
		NO_WINHOME=true
	fi

	if [[ -d "$WINDOWS_HOME/Documents" ]]; then
		! [[ -L "$HOME/WinDocs" ]] && ln -s "$WINDOWS_HOME/Documents" "$HOME/WinDocs"
	else
		_tutr_warn _warn_cannot_make_link "$WINDOWS_HOME/Documents"
		NO_WINDOCS=true
	fi

	if [[ -d "$WINDOWS_HOME/Downloads" ]]; then
		! [[ -L "$HOME/WinDownloads" ]] && ln -s "$WINDOWS_HOME/Downloads" "$HOME/WinDownloads"
	else
		_tutr_warn _warn_cannot_make_link "$WINDOWS_HOME/Downloads"
		NO_WINDOWNLOADS=true
	fi

	if [[ -d "$WINDOWS_HOME/Desktop" ]]; then
		! [[ -L "$HOME/WinDesktop" ]] && ln -s "$WINDOWS_HOME/Desktop" "$HOME/WinDesktop"
	else
		_tutr_warn _warn_cannot_make_link "$WINDOWS_HOME/Desktop"
		NO_WINDESKTOP=true
	fi

}

make_and_view_symlinks_test() {
	STUDENT_USED_ONE_INSTEAD_OF_L=99

	if [[ ${_CMD[0]} != ls && _tutr_nonce ]]; then
		return $PASS
	elif [[ ${_CMD[0]} == ls && ${_CMD[*]} == "-1" ]]; then
		# lol, student did 'ls -1' instead of 'ls -l'.
		#	... or should I say 1o1 
		return $STUDENT_USED_ONE_INSTEAD_OF_L
	else
		_tutr_generic_test -c ls -a "-l" -d "$HOME"
	fi
}

make_and_view_symlinks_hint() {
	case $1 in
		$PASS)
			;;
		$STUDENT_USED_ONE_INSTEAD_OF_L)
			cat <<-MSG
			LOL. That's an understandable mistake you have just made. You want to
			give the $(cmd ls) command the argument $(cmd -l) (that is a lowercase $(bld L)), not
			the argument $(cmd -1) (a $(bld one)).

			Try again!
			MSG
			;;
		*)
			_tutr_generic_hint $1 "ls -l" "$HOME"
			;;
	esac
}

make_and_view_symlinks_epilogue() {
	cat <<-MSG
	What you see in the output of $(cmd ls -l) is a bunch of additional
	information about each file present in this directory. What we're
	particularly concerned about is the symlinks $(path WinHome), $(path WinDocs),
	$(path WinDownloads), and $(path WinDesktop). You'll notice that on the right of
	each of those links is an arrow like so:
	 
	  $(ylw "LINK_NAME -> LINK_LOCATION")
	
	This tells us that the $(bld LINK_NAME) is a symlink pointing to
	$(bld LINK_LOCATION).
	MSG
	_tutr_pressanykey
}

cd_to_winhome_prologue() {
	cat <<-MSG
	Let's take a tour of our new symlinks and where they point to!

	Use $(cmd cd) to change directories into the symlink $(path WinHome) to visit the
	home directory for your Windows user.

	Remember, a symlink is like a "shortcut" that points to another file or
	directory. As this symlink points to a directory, we can just $(cmd cd) into
	it like we would do with any other directory.
	MSG
}

cd_to_winhome_test() {
	# Sigh... Apparently they don't have a Windows Home directory?
	[[ -n $NO_WINHOME || ! ( -L ~/WinHome && -d "$(readlink ~/WinHome)" ) ]] && return 0

	_tutr_nonce && return $PASS

	[[ "$PWD" == "$HOME/WinHome" ]] && return 0

	_tutr_generic_test -c cd -a "~/WinHome" -d "$HOME"
}

cd_to_winhome_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "cd ~/WinHome" "$HOME"
			;;
	esac
}

ls_in_winhome_prologue() {
	cat <<-MSG
	Let's take a look inside this directory. Run $(cmd ls) in this directory to
	view its contents.
	MSG
}

ls_in_winhome_test() {
	# Sigh... Apparently they don't have a Windows Home directory?
	[[ -n $NO_WINHOME || ! ( -L ~/WinHome && -d "$(readlink ~/WinHome)" ) ]] && return 0

	[[ ${_CMD[0]} != ls ]] && _tutr_nonce && return $PASS

	_tutr_generic_test -c ls -x -d "$HOME/WinHome" -i
}

ls_in_winhome_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "ls" "$HOME/WinHome"
			;;
	esac
}

ls_in_winhome_epilogue() {
	if (( $_RES != 0 )); then
		cat <<-MSG
		You likely got an error message in the output of the last command.
		I'm not exactly sure what caused that. Sometimes Windows really doesn't
		play nice with the way it creates certain files.

		Hopefully you were able to otherwise see the contents of $(path WinHome) fine.

		MSG
	fi

	DRIVE=$(getWinVars drive)

	cat <<-MSG
	This directory is the home directory for your Windows user. It contains
	all of the files that belong to *your* Windows user. This is where
	you'll find your $(path Documents), $(path Downloads), and $(path Desktop) directories,
	as well as any other files or programs installed for your Windows User.

	This is equivalent to:
	    $(path "$DRIVE\\Users\\<user_name>")
	if you were in the Windows file explorer.
	MSG
	_tutr_pressanykey
}

cd_to_windocs_prologue() {
	cat <<-MSG
	Let's check out the symlink to our Documents directory. 

	Use $(cmd cd) to change directories into the symlink $(path WinDocs), stored in
	your $(bld WSL home directory), to visit the Documents directory for your
	Windows user.
	MSG
}

cd_to_windocs_test() {
	# Sigh... Apparently they don't have a Windows Documents directory?
	[[ -n $NO_WINDOCS || ! ( -L ~/WinDocs && -d "$(readlink ~/WinDocs)" ) ]] && return 0

	_tutr_nonce && return $PASS

	[[ "$PWD" == "$HOME/WinDocs" ]] && return 0

	_tutr_generic_test -c cd -a "~/WinDocs"
}

cd_to_windocs_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "cd ~/WinDocs" "$PWD"
			;;
	esac
}

ls_in_windocs_prologue() {
	cat <<-MSG
	Let's take a look inside this directory. Run $(cmd ls) in this directory to
	view its contents.
	MSG
}

ls_in_windocs_test() {
	# Sigh... Apparently they don't have a Windows Documents directory?
	[[ -n $NO_WINDOCS || ! ( -L ~/WinDocs && -d "$(readlink ~/WinDocs)" ) ]] && return 0

	[[ ${_CMD[0]} != ls ]] && _tutr_nonce && return $PASS

	_tutr_generic_test -c ls -x -d "$HOME/WinDocs" -i
}

ls_in_windocs_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "ls" "$HOME/WinDocs"
			;;
	esac
}

ls_in_windocs_epilogue() {
	if (( $_RES != 0 )); then
		cat <<-MSG
		You likely got an error message in the output of the last command.
		I'm not exactly sure what caused that. Sometimes Windows really doesn't
		play nice with the way it creates certain files.
		
		Hopefully you were able to otherwise see the contents of $(path WinDocs) fine.

		MSG
	fi

	DRIVE=$(getWinVars drive)

	cat <<-MSG
	This directory is the $(path Documents) directory for your Windows user. It
	contains your personal data and other files you work with inside
	Windows.

	This is equivalent to:
	    $(path "$DRIVE\\Users\\<user_name>\\Documents")
	if you were in the Windows file explorer.

	This is the directory that I would recommend putting most of your work
	for CS1440 into. Personally, I like to make a $(path USU) directory right in
	the $(path Documents) directory, and then placing a $(path CS1440) directory there
	that contains all my work for this course.
	MSG
	_tutr_pressanykey
}

cd_to_windownloads_prologue() {
	cat <<-MSG
	Let's check out the symlink to our Downloads directory. 

	Use $(cmd cd) to change directories into the symlink $(path WinDownloads), stored
	in your $(bld WSL home directory), to visit the Downloads directory for your
	Windows user.
	MSG
}

cd_to_windownloads_test() {
	# Sigh... Apparently they don't have a Windows Downloads directory?
	[[ -n $NO_WINDOWNLOADS || ! ( -L ~/WinDownloads && -d "$(readlink ~/WinDownloads)" ) ]] && return 0

	_tutr_nonce && return $PASS

	[[ "$PWD" == "$HOME/WinDownloads" ]] && return 0

	_tutr_generic_test -c cd -a "~/WinDownloads"
}

cd_to_windownloads_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "cd ~/WinDownloads" "$PWD"
			;;
	esac
}

ls_in_windownloads_prologue() {
	cat <<-MSG
	Let's take a look inside this directory. Run $(cmd ls) in this directory to
	view its contents.
	MSG
}

ls_in_windownloads_test() {
	# Sigh... Apparently they don't have a Windows Downloads directory?
	[[ -n $NO_WINDOWNLOADS || ! ( -L ~/WinDownloads && -d "$(readlink ~/WinDownloads)" ) ]] && return 0

	[[ ${_CMD[0]} != ls ]] && _tutr_nonce && return $PASS

	_tutr_generic_test -c ls -x -d "$HOME/WinDownloads" -i
}

ls_in_windownloads_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "ls" "$HOME/WinDownloads"
			;;
	esac
}

ls_in_windownloads_epilogue() {
	if (( $_RES != 0 )); then
		cat <<-MSG
		You likely got an error message in the output of the last command.
		I'm not exactly sure what caused that. Sometimes Windows really doesn't
		play nice with the way it creates certain files.
		
		Hopefully you were able to otherwise see the contents of $(path WinDownloads)
		fine.

		MSG
	fi

	DRIVE=$(getWinVars drive)

	cat <<-MSG
	This directory is the $(path Downloads) directory for your Windows user. It
	is the default location for files you download from your web browser or
	other programs inside Windows.

	This is equivalent to:
	    $(path "$DRIVE\\Users\\<user_name>\\Downloads")
	if you were in the Windows file explorer.
	MSG
	_tutr_pressanykey
}

cd_to_windesktop_prologue() {
	cat <<-MSG
	Let's check out the symlink to our $(path Desktop) directory. 

	Use $(cmd cd) to change directories into the symlink $(path WinDesktop), stored
	in your $(bld WSL home directory), to visit the $(path Desktop) directory for your
	Windows user.
	MSG
}

cd_to_windesktop_test() {
	# Sigh... Apparently they don't have a Windows Desktop directory? OneDrive...
	[[ -n $NO_WINDESKTOP || ! ( -L ~/WinDesktop && -d "$(readlink ~/WinDesktop)" ) ]] && return 0

	_tutr_nonce && return $PASS

	[[ "$PWD" == "$HOME/WinDesktop" ]] && return 0

	_tutr_generic_test -c cd -a "~/WinDesktop"
}

cd_to_windesktop_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "cd ~/WinDesktop" "$PWD"
			;;
	esac
}

ls_in_windesktop_prologue() {
	cat <<-MSG
	Let's take a look inside this directory. Run $(cmd ls) in this directory to
	view its contents.
	MSG
}

ls_in_windesktop_test() {
	# Sigh... Apparently they don't have a Windows Desktop directory? OneDrive...
	[[ -n $NO_WINDESKTOP || ! ( -L ~/WinDesktop && -d "$(readlink ~/WinDesktop)" ) ]] && return 0

	[[ ${_CMD[0]} != ls ]] && _tutr_nonce && return $PASS

	_tutr_generic_test -c ls -x -d "$HOME/WinDesktop" -i
}

ls_in_windesktop_hint() {
	case $1 in
		$PASS)
			;;
		*)
			_tutr_generic_hint $1 "ls" "$HOME/WinDesktop"
			;;
	esac
}

ls_in_windesktop_epilogue() {
	if (( $_RES != 0 )); then
		cat <<-MSG
		You likely got an error message in the output of the last command.
		I'm not exactly sure what caused that. Sometimes Windows really doesn't
		play nice with the way it creates certain files.
		
		Hopefully you were able to otherwise see the contents of $(path WinDesktop)
		fine.

		MSG
	fi

	DRIVE=$(getWinVars drive)

	cat <<-MSG
	This directory is the $(path Desktop) directory for your Windows user. It is
	where the files located on your Windows $(path Desktop) are located.

	This is equivalent to:
	    $(path "$DRIVE\\Users\\<user_name>\\Desktop")
	if you were in the Windows file explorer.
	MSG
	_tutr_pressanykey
}

source main.sh && _tutr_begin \
	wsl_installed_python \
	check_python_installed \
	setup_python_link \
	show_windows_filesystem \
	move_to_home \
	make_and_view_symlinks \
	cd_to_winhome \
	ls_in_winhome \
	cd_to_windocs \
	ls_in_windocs \
	cd_to_windownloads \
	ls_in_windownloads \
	cd_to_windesktop \
	ls_in_windesktop
	
# vim: set filetype=sh noexpandtab tabstop=4 shiftwidth=4 textwidth=76 colorcolumn=76:
